package ru.evstigneev.tm.util;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.dto.ProjectDto;
import ru.evstigneev.tm.dto.SessionDto;
import ru.evstigneev.tm.dto.TaskDto;
import ru.evstigneev.tm.dto.UserDto;
import ru.evstigneev.tm.entity.Project;
import ru.evstigneev.tm.entity.Session;
import ru.evstigneev.tm.entity.Task;
import ru.evstigneev.tm.entity.User;
import ru.evstigneev.tm.service.PropertyService;

import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

public class HibernateUtil {

    @NotNull
    private final PropertyService propertyService = new PropertyService();

    public EntityManagerFactory factory() {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER, propertyService.getDriver());
        settings.put(Environment.URL, propertyService.getUrl());
        settings.put(Environment.USER, propertyService.getDbLogin());
        settings.put(Environment.PASS, propertyService.getDbPassword());
        settings.put(Environment.DIALECT,
                "org.hibernate.dialect.MySQL5InnoDBDialect");
        settings.put(Environment.HBM2DDL_AUTO, "update");
        settings.put(Environment.SHOW_SQL, "true");
        @NotNull final StandardServiceRegistryBuilder registryBuilder
                = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(TaskDto.class);
        sources.addAnnotatedClass(ProjectDto.class);
        sources.addAnnotatedClass(UserDto.class);
        sources.addAnnotatedClass(SessionDto.class);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(User.class);
        sources.addAnnotatedClass(Session.class);
        @NotNull final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

}
