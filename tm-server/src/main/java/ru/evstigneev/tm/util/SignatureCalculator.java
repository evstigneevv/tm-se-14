package ru.evstigneev.tm.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.exception.EmptyStringException;
import ru.evstigneev.tm.service.PropertyService;

public class SignatureCalculator {

    @NotNull
    private static final PropertyService propertyService = new PropertyService();

    public static String calculateSignature(@NotNull final Object value) throws Exception {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String valueString = objectMapper.writeValueAsString(value);
        return calculateSignature(valueString);
    }

    private static String calculateSignature(@NotNull final String value) throws Exception {
        if (value.isEmpty()) {
            throw new EmptyStringException();
        }
        @Nullable final String salt = propertyService.getSalt();
        @Nullable final int cycle = propertyService.getCycle();
        @NotNull String result = value;
        for (int i = 0; i < cycle; i++) {
            result = PasswordParser.getPasswordHash(salt + result + salt);
        }
        return result;
    }

}