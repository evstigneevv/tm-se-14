package ru.evstigneev.tm.util;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.service.PropertyService;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnector {

    @NotNull
    private static final PropertyService propertyService = new PropertyService();

    public static Connection getConnection() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            @NotNull final String dbType = propertyService.getDbType();
            @NotNull final String dbHost = propertyService.getDbHost();
            @NotNull final String dbPort = propertyService.getDbPort();
            @NotNull final String dbName = propertyService.getDbName();
            @NotNull final String dbLogin = propertyService.getDbLogin();
            @NotNull final String dbPassword = propertyService.getDbPassword();
            @NotNull final String url = dbType + "://" + dbHost + ":" + dbPort + "/" + dbName;
            @NotNull final Connection connection = DriverManager.getConnection(url, dbLogin, dbPassword);
            return connection;
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

}
