package ru.evstigneev.tm.wrapper;

import lombok.Getter;
import lombok.Setter;
import ru.evstigneev.tm.dto.UserDto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collection;

@XmlRootElement(name = "users")
@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
public class Users {

    private Collection<UserDto> users;

}
