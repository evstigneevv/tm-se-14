package ru.evstigneev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.api.service.ITaskService;
import ru.evstigneev.tm.comparator.ComparatorTaskDateCreation;
import ru.evstigneev.tm.comparator.ComparatorTaskDateFinish;
import ru.evstigneev.tm.comparator.ComparatorTaskDateStart;
import ru.evstigneev.tm.dto.TaskDto;
import ru.evstigneev.tm.entity.Project;
import ru.evstigneev.tm.entity.Task;
import ru.evstigneev.tm.entity.User;
import ru.evstigneev.tm.enumerated.Status;
import ru.evstigneev.tm.exception.CommandCorruptException;
import ru.evstigneev.tm.exception.EmptyStringException;
import ru.evstigneev.tm.repository.TaskRepository;
import ru.evstigneev.tm.util.DateParser;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.*;

public class TaskService extends AbstractService<TaskDto> implements ITaskService {

    @NotNull
    private static final String DATE_START_COMPARATOR_NAME = "date start";
    @NotNull
    private static final String DATE_FINISH_COMPARATOR_NAME = "date finish";
    @NotNull
    private static final String DATE_CREATION_COMPARATOR_NAME = "date creation";
    @NotNull
    private static final String STATUS_COMPARATOR_NAME = "status";

    @NotNull
    private final EntityManagerFactory entityManagerFactory;

    public TaskService(@NotNull final EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    @Override
    public void create(@NotNull final String userId, @NotNull final String projectId, @NotNull final String name,
                       @NotNull final String description) throws Exception {
        if (userId.isEmpty() || projectId.isEmpty() || name.isEmpty() || description.isEmpty())
            throw new EmptyStringException();
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
        @NotNull final Task task = new Task();
        task.setId(UUID.randomUUID().toString());
        @NotNull final User user = entityManager.find(User.class, userId);
        task.setUser(user);
        @NotNull final Project project = entityManager.find(Project.class, projectId);
        task.setProject(project);
        task.setName(name);
        task.setDescription(description);
        task.setDateOfCreation(new Date());
        task.setStatus(Status.PLANNING);
        entityManager.getTransaction().begin();
        taskRepository.persist(task);
        entityManager.getTransaction().commit();
    }

    @Override
    public Collection<TaskDto> findAll() {
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
        return createProjectDtoList(taskRepository.findAll());
    }

    @Override
    public Collection<TaskDto> findAllByUserId(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new EmptyStringException();
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
        return createProjectDtoList(taskRepository.findAllByUserId(userId));
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final String taskId) throws Exception {
        if (userId.isEmpty() || taskId.isEmpty()) throw new EmptyStringException();
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
        entityManager.getTransaction().begin();
        taskRepository.remove(userId, taskId);
        entityManager.getTransaction().commit();
    }

    @Override
    public Collection<TaskDto> getTaskListByProjectId(@NotNull final String userId, @NotNull final String projectId) throws Exception {
        if (userId.isEmpty() || projectId.isEmpty()) throw new EmptyStringException();
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
        return createProjectDtoList(taskRepository.getTaskListByProjectId(userId, projectId));
    }

    @Override
    public void update(@NotNull final String userId, @NotNull final String taskId,
                       @NotNull final String name, @Nullable final String description,
                       @Nullable final String dateStart, @Nullable final String dateFinish,
                       @NotNull final Status status) throws Exception {
        if (userId.isEmpty() || taskId.isEmpty() || name.isEmpty()) {
            throw new EmptyStringException();
        }
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
        @NotNull final Task task = taskRepository.findOne(userId, taskId);
        task.setName(name);
        task.setDescription(description);
        task.setDateStart(DateParser.setDateByString(dateStart));
        task.setDateFinish(DateParser.setDateByString(dateFinish));
        task.setStatus(status);
        entityManager.getTransaction().begin();
        taskRepository.update(task);
        entityManager.getTransaction().commit();
    }

    @Override
    public void merge(@NotNull final String userId, @NotNull final TaskDto taskDto) {
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
        taskRepository.merge(convertDtoToTask(taskDto));
    }

    @Override
    public void persist(@NotNull final String userId, @NotNull final TaskDto taskDto) {
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
        entityManager.getTransaction().begin();
        taskRepository.persist(convertDtoToTask(taskDto));
        entityManager.getTransaction().commit();
    }

    @Override
    public void removeAll() {
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
        entityManager.getTransaction().begin();
        taskRepository.removeAll();
        entityManager.getTransaction().commit();
    }

    @Override
    public void removeAllByUserId(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new EmptyStringException();
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
        entityManager.getTransaction().begin();
        taskRepository.removeAllByUserId(userId);
        entityManager.getTransaction().commit();
    }

    @Override
    public void deleteAllProjectTasks(@NotNull final String userId, @NotNull final String projectId) throws
            Exception {
        if (userId.isEmpty() || projectId.isEmpty()) throw new EmptyStringException();
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
        entityManager.getTransaction().begin();
        taskRepository.deleteAllProjectTasks(userId, projectId);
        entityManager.getTransaction().commit();
    }

    @Override
    public TaskDto findOne(@NotNull final String userId, @NotNull final String projectId,
                           @NotNull final String taskId) throws Exception {
        if (userId.isEmpty() || projectId.isEmpty()) throw new EmptyStringException();
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
        return convertTaskToDto(taskRepository.findOne(userId, projectId));
    }

    public List<TaskDto> sort(@NotNull final String comparatorName) throws Exception {
        @NotNull final List<TaskDto> sortedList = new ArrayList<>(findAll());
        @Nullable final Comparator<TaskDto> comparator = findComparator(comparatorName);
        if (comparator == null) {
            Collections.sort(sortedList);
            return sortedList;
        }
        Collections.sort(sortedList, comparator);
        return sortedList;
    }

    private Comparator<TaskDto> findComparator(@NotNull final String name) throws CommandCorruptException {
        switch (name.toLowerCase()) {
            default:
                throw new CommandCorruptException();
            case DATE_START_COMPARATOR_NAME: {
                return new ComparatorTaskDateStart();
            }
            case DATE_FINISH_COMPARATOR_NAME: {
                return new ComparatorTaskDateFinish();
            }
            case DATE_CREATION_COMPARATOR_NAME: {
                return new ComparatorTaskDateCreation();
            }
            case STATUS_COMPARATOR_NAME: {
                return null;
            }
        }
    }

    public List<TaskDto> searchByString(@NotNull final String string) throws Exception {
        if (string.isEmpty()) throw new EmptyStringException();
        @Nullable final List<TaskDto> projectList = new ArrayList<>();
        for (TaskDto taskDto : findAll()) {
            if (taskDto.getName().contains(string) || (taskDto.getDescription() != null && taskDto.getDescription().contains(string))) {
                projectList.add(taskDto);
            }
        }
        return projectList;
    }

    private TaskDto convertTaskToDto(@NotNull final Task task) {
        @NotNull final TaskDto taskDto = new TaskDto();
        taskDto.setId(task.getId());
        taskDto.setUserId(task.getUser().getId());
        taskDto.setProjectId(task.getProject().getId());
        taskDto.setName(task.getName());
        taskDto.setDescription(task.getDescription());
        taskDto.setDateOfCreation(task.getDateOfCreation());
        taskDto.setDateStart(task.getDateStart());
        taskDto.setDateFinish(task.getDateFinish());
        taskDto.setStatus(task.getStatus());
        return taskDto;
    }

    private Task convertDtoToTask(@NotNull final TaskDto taskDto) {
        @NotNull final Task task = new Task();
        task.setId(taskDto.getId());
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        task.setUser(entityManager.find(User.class, taskDto.getUserId()));
        task.setName(taskDto.getName());
        task.setDescription(taskDto.getDescription());
        task.setDateOfCreation(taskDto.getDateOfCreation());
        task.setDateStart(taskDto.getDateStart());
        task.setDateFinish(taskDto.getDateFinish());
        task.setStatus(taskDto.getStatus());
        return task;
    }

    private Collection<TaskDto> createProjectDtoList(@NotNull final Collection<Task> taskList) {
        @NotNull final Collection<TaskDto> taskDtoList = new ArrayList<>();
        for (Task task : taskList) {
            taskDtoList.add(convertTaskToDto(task));
        }
        return taskDtoList;
    }

}
