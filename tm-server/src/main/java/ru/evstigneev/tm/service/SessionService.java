package ru.evstigneev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.api.service.ISessionService;
import ru.evstigneev.tm.dto.SessionDto;
import ru.evstigneev.tm.entity.Session;
import ru.evstigneev.tm.entity.User;
import ru.evstigneev.tm.enumerated.RoleType;
import ru.evstigneev.tm.exception.EmptyStringException;
import ru.evstigneev.tm.exception.LoginException;
import ru.evstigneev.tm.exception.NoPermissionException;
import ru.evstigneev.tm.repository.SessionRepository;
import ru.evstigneev.tm.repository.UserRepository;
import ru.evstigneev.tm.util.SignatureCalculator;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.UUID;

public class SessionService implements ISessionService {

    @NotNull
    private static final long SESSION_EXPIRATION_TIME = 1_800_000L;

    @NotNull
    private final EntityManagerFactory entityManagerFactory;

    public SessionService(@NotNull final EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    @Override
    public void validate(@Nullable final SessionDto session) throws Exception {
        if (session == null) throw new LoginException();
        if (session.getSignature() == null || session.getId().isEmpty() ||
                session.getRoleType().displayName().isEmpty() || session.getSignature().isEmpty()) {
            throw new LoginException();
        }
        if (System.currentTimeMillis() - session.getTimestamp() > SESSION_EXPIRATION_TIME) {
            throw new LoginException();
        }
        @NotNull final SessionDto temp = session.clone();
        temp.setSignature(null);
        temp.setSignature(SignatureCalculator.calculateSignature(temp));
        if (temp.getSignature() != null && !temp.getSignature().equals(session.getSignature())) {
            throw new NoPermissionException();
        }
    }

    @Override
    public void validate(@Nullable final SessionDto session, @NotNull final RoleType roleType) throws Exception {
        validate(session);
        if (!session.getRoleType().equals(roleType)) throw new NoPermissionException();
    }

    @Override
    public SessionDto openSession(@NotNull final String login, @NotNull final String password) throws Exception {
        if (login.isEmpty() || password.isEmpty()) throw new EmptyStringException();
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final UserRepository userRepository = new UserRepository(entityManager);
        @NotNull final SessionRepository sessionRepository = new SessionRepository(entityManager);
        @NotNull final SessionDto session = new SessionDto();
        @NotNull final User user = userRepository.findByLogin(login);
        session.setId(UUID.randomUUID().toString());
        session.setUserId(user.getId());
        session.setRoleType(user.getRole());
        session.setTimestamp(System.currentTimeMillis());
        session.setSignature(SignatureCalculator.calculateSignature(session));
        entityManager.getTransaction().begin();
        sessionRepository.persist(convertDtoToSession(session));
        entityManager.getTransaction().commit();
        return session;
    }

    @Override
    public boolean closeSession(@NotNull final SessionDto session) {
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final SessionRepository sessionRepository = new SessionRepository(entityManager);
        entityManager.getTransaction().begin();
        sessionRepository.remove(convertDtoToSession(session));
        entityManager.getTransaction().commit();
        return true;
    }

    private SessionDto convertSessionToDto(@NotNull final Session session) {
        @NotNull final SessionDto sessionDto = new SessionDto();
        sessionDto.setId(session.getId());
        sessionDto.setUserId(session.getUser().getId());
        sessionDto.setRoleType(session.getRoleType());
        sessionDto.setTimestamp(session.getTimestamp());
        sessionDto.setSignature(session.getSignature());
        return sessionDto;
    }

    private Session convertDtoToSession(@NotNull final SessionDto sessionDto) {
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final Session session = new Session();
        session.setId(sessionDto.getId());
        session.setUser(entityManager.find(User.class, sessionDto.getUserId()));
        session.setRoleType(sessionDto.getRoleType());
        session.setTimestamp(sessionDto.getTimestamp());
        session.setSignature(sessionDto.getSignature());
        return session;
    }

}