package ru.evstigneev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.api.service.IUserService;
import ru.evstigneev.tm.dto.UserDto;
import ru.evstigneev.tm.entity.User;
import ru.evstigneev.tm.enumerated.RoleType;
import ru.evstigneev.tm.exception.EmptyStringException;
import ru.evstigneev.tm.repository.UserRepository;
import ru.evstigneev.tm.util.PasswordParser;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

public class UserService extends AbstractService implements IUserService {

    @NotNull
    private final EntityManagerFactory entityManagerFactory;

    public UserService(@NotNull final EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    @Override
    public void persist(@NotNull final String login, @NotNull final String password) throws Exception {
        if (login.isEmpty() || password.isEmpty()) throw new EmptyStringException();
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final UserRepository userRepository = new UserRepository(entityManager);
        @NotNull final User user = new User();
        user.setId(UUID.randomUUID().toString());
        user.setLogin(login);
        user.setPassword(PasswordParser.getPasswordHash(password));
        user.setRole(RoleType.ADMIN);
        entityManager.getTransaction().begin();
        userRepository.persist(user);
        entityManager.getTransaction().commit();
    }

    @Override
    public void persist(@NotNull final String login, @NotNull final String password, @NotNull final RoleType role) throws Exception {
        if (login.isEmpty() || password.isEmpty()) throw new EmptyStringException();
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final UserRepository userRepository = new UserRepository(entityManager);
        @NotNull final User user = new User();
        user.setId(UUID.randomUUID().toString());
        user.setLogin(login);
        user.setPassword(PasswordParser.getPasswordHash(password));
        user.setRole(role);
        entityManager.getTransaction().begin();
        userRepository.persist(user);
        entityManager.getTransaction().commit();
    }

    @Override
    public void persist(@NotNull final UserDto user) {
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final UserRepository userRepository = new UserRepository(entityManager);
        entityManager.getTransaction().begin();
        userRepository.persist(convertDtoToUser(user));
        entityManager.getTransaction().commit();
    }

    @Override
    public Collection<UserDto> findAll() {
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final UserRepository userRepository = new UserRepository(entityManager);
        return createUserDtoList(userRepository.findAll());
    }

    @Override
    public boolean checkPassword(@NotNull final String login, @NotNull final String password) throws Exception {
        if (login.isEmpty() || password.isEmpty()) throw new EmptyStringException();
        @Nullable final Collection<UserDto> users = findAll();
        if (users != null) {
            for (UserDto user : users) {
                if (user.getLogin().equals(login)) {
                    return PasswordParser.getPasswordHash(password).equals(user.getPassword());
                }
            }
        }
        return false;
    }

    @Override
    public boolean checkPasswordByUserId(@NotNull final String userId, @NotNull final String password) throws Exception {
        if (userId.isEmpty() || password.isEmpty()) throw new EmptyStringException();
        @Nullable final Collection<UserDto> users = findAll();
        if (users != null) {
            for (UserDto user : users) {
                if (user.getId().equals(userId)) {
                    return PasswordParser.getPasswordHash(password).equals(user.getPassword());
                }
            }
        }
        return false;
    }

    @Override
    public void updatePassword(@NotNull final String userId, @NotNull final String newPassword) throws Exception {
        if (userId.isEmpty() || newPassword.isEmpty()) throw new EmptyStringException();
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final UserRepository userRepository = new UserRepository(entityManager);
        entityManager.getTransaction().begin();
        userRepository.updatePassword(userId, PasswordParser.getPasswordHash(newPassword));
        entityManager.getTransaction().commit();
    }

    @Override
    public UserDto findByLogin(@NotNull final String login) throws Exception {
        if (login.isEmpty()) throw new EmptyStringException();
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final UserRepository userRepository = new UserRepository(entityManager);
        return convertUserToDto(userRepository.findByLogin(login));
    }

    @Override
    public void update(@NotNull final String userId, @NotNull final String login, @NotNull final String password,
                       @NotNull final RoleType role) throws Exception {
        if (userId.isEmpty() || login.isEmpty() || password.isEmpty()) throw new EmptyStringException();
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final UserRepository userRepository = new UserRepository(entityManager);
        entityManager.getTransaction().begin();
        userRepository.update(userId, login, password, role);
        entityManager.getTransaction().commit();
    }

    @Override
    public void remove(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new EmptyStringException();
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final UserRepository userRepository = new UserRepository(entityManager);
        entityManager.getTransaction().begin();
        userRepository.remove(userId);
        entityManager.getTransaction().commit();
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final UserRepository userRepository = new UserRepository(entityManager);
        entityManager.getTransaction().begin();
        userRepository.removeAll();
        entityManager.getTransaction().commit();
    }

    @Override
    public void merge(@NotNull final UserDto userDto) {
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final UserRepository userRepository = new UserRepository(entityManager);
        userRepository.merge(convertDtoToUser(userDto));
    }

    private UserDto convertUserToDto(@NotNull final User user) {
        @NotNull final UserDto userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setLogin(user.getLogin());
        userDto.setPassword(user.getPassword());
        userDto.setRole(user.getRole());
        return userDto;
    }

    private User convertDtoToUser(@NotNull final UserDto userDto) {
        @NotNull final User user = new User();
        user.setId(userDto.getId());
        user.setLogin(userDto.getLogin());
        user.setPassword(userDto.getPassword());
        user.setRole(userDto.getRole());
        return user;
    }

    private List<UserDto> createUserDtoList(@NotNull final List<User> userList) {
        @NotNull final List<UserDto> userDtoList = new ArrayList<>();
        for (User user : userList) {
            userDtoList.add(convertUserToDto(user));
        }
        return userDtoList;
    }

}