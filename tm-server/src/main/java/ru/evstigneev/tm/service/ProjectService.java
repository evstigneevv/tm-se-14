package ru.evstigneev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.api.service.IProjectService;
import ru.evstigneev.tm.comparator.ComparatorProjectDateCreation;
import ru.evstigneev.tm.comparator.ComparatorProjectDateFinish;
import ru.evstigneev.tm.comparator.ComparatorProjectDateStart;
import ru.evstigneev.tm.dto.ProjectDto;
import ru.evstigneev.tm.entity.Project;
import ru.evstigneev.tm.entity.User;
import ru.evstigneev.tm.enumerated.Status;
import ru.evstigneev.tm.exception.CommandCorruptException;
import ru.evstigneev.tm.exception.EmptyStringException;
import ru.evstigneev.tm.repository.ProjectRepository;
import ru.evstigneev.tm.repository.UserRepository;
import ru.evstigneev.tm.util.DateParser;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.*;

public class ProjectService extends AbstractService<ProjectDto> implements IProjectService {

    @NotNull
    private static final String DATE_START_COMPARATOR_NAME = "date start";
    @NotNull
    private static final String DATE_FINISH_COMPARATOR_NAME = "date finish";
    @NotNull
    private static final String DATE_CREATION_COMPARATOR_NAME = "date creation";
    @NotNull
    private static final String STATUS_COMPARATOR_NAME = "status";

    @NotNull
    private final EntityManagerFactory entityManagerFactory;

    public ProjectService(@NotNull final EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    @Override
    public Collection<ProjectDto> findAll() {
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        return createProjectDtoList(projectRepository.findAll());
    }

    @Override
    public void create(@NotNull final String userId, @NotNull final String projectName,
                       @NotNull final String description) throws Exception {
        if (userId.isEmpty() || projectName.isEmpty() || description.isEmpty()) throw new EmptyStringException();
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        @NotNull final UserRepository userRepository = new UserRepository(entityManager);
        @NotNull final Project project = new Project();
        project.setId(UUID.randomUUID().toString());
        @NotNull final User user = userRepository.findByUserId(userId);
        project.setUser(user);
        project.setName(projectName);
        project.setDescription(description);
        project.setDateOfCreation(new Date());
        project.setStatus(Status.PLANNING);
        entityManager.getTransaction().begin();
        projectRepository.persist(project);
        entityManager.getTransaction().commit();
    }

    @Override
    public void persist(@NotNull final ProjectDto project) {
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        entityManager.getTransaction().begin();
        projectRepository.persist(convertDtoToProject(project));
        entityManager.getTransaction().commit();
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final String projectId) throws Exception {
        if (userId.isEmpty() || projectId.isEmpty()) throw new EmptyStringException();
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        entityManager.getTransaction().begin();
        projectRepository.remove(userId, projectId);
        entityManager.getTransaction().commit();
    }

    @Override
    public void update(@NotNull final String userId, @NotNull final String projectId,
                       @NotNull final String name, @Nullable final String description,
                       @Nullable final String dateStart, @Nullable final String dateFinish,
                       @NotNull final Status status) throws Exception {
        if (userId.isEmpty() || projectId.isEmpty() || name.isEmpty()) {
            throw new EmptyStringException();
        }
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        @NotNull final Project project = projectRepository.findOne(userId, projectId);
        project.setName(name);
        project.setDescription(description);
        project.setDateStart(DateParser.setDateByString(dateStart));
        project.setDateFinish(DateParser.setDateByString(dateFinish));
        project.setStatus(status);
        entityManager.getTransaction().begin();
        projectRepository.update(project);
        entityManager.getTransaction().commit();
    }

    @Override
    public ProjectDto findOne(@NotNull final String userId, @NotNull final String projectId) throws Exception {
        if (userId.isEmpty() || projectId.isEmpty()) throw new EmptyStringException();
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        return convertProjectToDto(projectRepository.findOne(userId, projectId));
    }

    @Override
    public Collection<ProjectDto> findAllByUserId(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new EmptyStringException();
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        return createProjectDtoList(projectRepository.findAllByUserId(userId));
    }

    @Override
    public void removeAll() {
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        entityManager.getTransaction().begin();
        projectRepository.removeAll();
        entityManager.getTransaction().commit();
    }

    @Override
    public void removeAllByUserId(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new EmptyStringException();
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        entityManager.getTransaction().begin();
        projectRepository.removeAllByUserId(userId);
        entityManager.getTransaction().commit();
    }

    @Override
    public void merge(ProjectDto project) {
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        projectRepository.merge(convertDtoToProject(project));
    }

    public List<ProjectDto> sort(@NotNull final String comparatorName) throws Exception {
        @NotNull final List<ProjectDto> sortedList = new ArrayList<>(findAll());
        @Nullable final Comparator<ProjectDto> comparator = findComparator(comparatorName);
        if (comparator == null) {
            Collections.sort(sortedList);
            return sortedList;
        }
        Collections.sort(sortedList, comparator);
        return sortedList;
    }

    private Comparator<ProjectDto> findComparator(@NotNull final String name) throws CommandCorruptException {
        switch (name.toLowerCase()) {
            default:
                throw new CommandCorruptException();
            case DATE_START_COMPARATOR_NAME: {
                return new ComparatorProjectDateStart();
            }
            case DATE_FINISH_COMPARATOR_NAME: {
                return new ComparatorProjectDateFinish();
            }
            case DATE_CREATION_COMPARATOR_NAME: {
                return new ComparatorProjectDateCreation();
            }
            case STATUS_COMPARATOR_NAME: {
                return null;
            }
        }
    }

    public List<ProjectDto> searchByString(@NotNull final String string) throws Exception {
        if (string.isEmpty()) throw new EmptyStringException();
        @NotNull final List<ProjectDto> projectList = new ArrayList<>();
        for (ProjectDto project : findAll()) {
            if (project.getName().contains(string) || (project.getDescription() != null && project.getDescription().contains(string))) {
                projectList.add(project);
            }
        }
        return projectList;
    }

    private ProjectDto convertProjectToDto(@NotNull final Project project) {
        @NotNull final ProjectDto projectDto = new ProjectDto();
        projectDto.setId(project.getId());
        projectDto.setUserId(project.getUser().getId());
        projectDto.setName(project.getName());
        projectDto.setDescription(project.getDescription());
        projectDto.setDateOfCreation(project.getDateOfCreation());
        projectDto.setDateStart(project.getDateStart());
        projectDto.setDateFinish(project.getDateFinish());
        projectDto.setStatus(project.getStatus());
        return projectDto;
    }

    private Project convertDtoToProject(@NotNull final ProjectDto projectDto) {
        @NotNull final Project project = new Project();
        project.setId(projectDto.getId());
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        project.setUser(entityManager.find(User.class, projectDto.getUserId()));
        project.setName(projectDto.getName());
        project.setDescription(projectDto.getDescription());
        project.setDateOfCreation(projectDto.getDateOfCreation());
        project.setDateStart(projectDto.getDateStart());
        project.setDateFinish(projectDto.getDateFinish());
        project.setStatus(projectDto.getStatus());
        return project;
    }

    private Collection<ProjectDto> createProjectDtoList(@NotNull final Collection<Project> projectList) {
        @NotNull final Collection<ProjectDto> projectDtoList = new ArrayList<>();
        for (Project project : projectList) {
            projectDtoList.add(convertProjectToDto(project));
        }
        return projectDtoList;
    }

}
