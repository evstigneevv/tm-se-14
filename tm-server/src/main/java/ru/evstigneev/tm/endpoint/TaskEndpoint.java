package ru.evstigneev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.api.endpoint.ITaskEndpoint;
import ru.evstigneev.tm.api.service.IProjectService;
import ru.evstigneev.tm.api.service.ISessionService;
import ru.evstigneev.tm.api.service.ITaskService;
import ru.evstigneev.tm.dto.ProjectDto;
import ru.evstigneev.tm.dto.SessionDto;
import ru.evstigneev.tm.dto.TaskDto;
import ru.evstigneev.tm.enumerated.RoleType;
import ru.evstigneev.tm.enumerated.Status;
import ru.evstigneev.tm.exception.RepositoryException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService(endpointInterface = "ru.evstigneev.tm.api.endpoint.ITaskEndpoint")
public class TaskEndpoint implements ITaskEndpoint {

    @NotNull
    private ITaskService taskService;
    @NotNull
    private IProjectService projectService;
    @NotNull
    private ISessionService sessionService;

    public TaskEndpoint() {
    }

    public TaskEndpoint(@NotNull final ITaskService taskService, @NotNull final IProjectService projectService,
                        @NotNull final ISessionService sessionService) {
        this.taskService = taskService;
        this.projectService = projectService;
        this.sessionService = sessionService;
    }

    @Override
    @WebMethod
    public void createTask(@WebParam(name = "session") @Nullable final SessionDto session,
                           @WebParam(name = "projectId") @NotNull final String projectId,
                           @WebParam(name = "taskName") @NotNull final String taskName,
                           @WebParam(name = "description") @Nullable final String description) throws Exception {
        sessionService.validate(session);
        for (ProjectDto project : projectService.findAllByUserId(session.getUserId())) {
            System.out.println();
            System.out.println(projectId);
            System.out.println(project.getId());
            System.out.println();
            if (project.getId().equals(projectId)) {
                taskService.create(session.getUserId(), projectId, taskName, description);
                return;
            }
        }
        throw new RepositoryException();
    }

    @Override
    @WebMethod
    public Collection<TaskDto> findAllTasks(@WebParam(name = "session") @Nullable final SessionDto session) throws Exception {
        sessionService.validate(session, RoleType.ADMIN);
        return taskService.findAll();
    }

    @Override
    @WebMethod
    public Collection<TaskDto> findAllTasksByUserId(@WebParam(name = "session") @Nullable final SessionDto session) throws Exception {
        sessionService.validate(session);
        return taskService.findAllByUserId(session.getUserId());
    }

    @Override
    @WebMethod
    public void removeTask(@WebParam(name = "session") @Nullable final SessionDto session,
                           @WebParam(name = "taskId") @NotNull final String taskId) throws Exception {
        sessionService.validate(session);
        taskService.remove(session.getUserId(), taskId);
    }

    @Override
    @WebMethod
    public Collection<TaskDto> getTaskListByProjectId(@WebParam(name = "session") @Nullable final SessionDto session,
                                                      @WebParam(name = "projectId") @NotNull final String projectId) throws Exception {
        sessionService.validate(session);
        return taskService.getTaskListByProjectId(session.getUserId(), projectId);
    }

    @Override
    @WebMethod
    public void updateTask(@WebParam(name = "session") @Nullable final SessionDto session,
                           @WebParam(name = "taskId") @NotNull final String taskId,
                           @WebParam(name = "taskName") @NotNull final String taskName,
                           @WebParam(name = "description") @Nullable final String description,
                           @WebParam(name = "dateStart") @Nullable final String dateStart,
                           @WebParam(name = "dateFinish") @Nullable final String dateFinish,
                           @WebParam(name = "status") @NotNull final Status status) throws Exception {
        sessionService.validate(session);
        taskService.update(session.getUserId(), taskId, taskName, description, dateStart, dateFinish, status);
    }

    @Override
    @WebMethod
    public void mergeTask(@WebParam(name = "session") @Nullable final SessionDto session,
                          @WebParam(name = "task") @NotNull final TaskDto taskDto) throws Exception {
        sessionService.validate(session);
        taskService.merge(session.getUserId(), taskDto);
    }

    @Override
    @WebMethod
    public void persistTask(@WebParam(name = "session") @Nullable final SessionDto session,
                            @WebParam(name = "task") @NotNull final TaskDto taskDto) throws Exception {
        sessionService.validate(session);
        taskService.persist(session.getUserId(), taskDto);
    }

    @Override
    @WebMethod
    public void removeAllTasks(@WebParam(name = "session") @Nullable final SessionDto session) throws Exception {
//        sessionService.validate(session, RoleType.ADMIN);
        taskService.removeAll();
    }

    @Override
    @WebMethod
    public void removeAllTasksByUserId(@WebParam(name = "session") @Nullable final SessionDto session) throws Exception {
        sessionService.validate(session);
        taskService.removeAllByUserId(session.getUserId());
    }

    @Override
    @WebMethod
    public void deleteAllProjectTasks(@WebParam(name = "session") @Nullable final SessionDto session,
                                      @WebParam(name = "projectId") @NotNull final String projectId) throws Exception {
        sessionService.validate(session);
        taskService.deleteAllProjectTasks(session.getUserId(), projectId);
    }

    @Override
    @WebMethod
    public TaskDto findOne(@WebParam(name = "session") @Nullable final SessionDto session,
                           @WebParam(name = "projectId") @NotNull final String projectId,
                           @WebParam(name = "taskId") @NotNull final String taskId) throws Exception {
        sessionService.validate(session);
        return taskService.findOne(session.getUserId(), projectId, taskId);
    }

    @Override
    @WebMethod
    public List<TaskDto> sortTasks(@WebParam(name = "session") @Nullable final SessionDto session,
                                   @WebParam(name = "comparatorName") @NotNull final String comparatorName) throws Exception {
        sessionService.validate(session);
        return taskService.sort(comparatorName);
    }

    @Override
    @WebMethod
    public List<TaskDto> searchTaskByString(@WebParam(name = "session") @Nullable final SessionDto session,
                                            @WebParam(name = "string") @NotNull final String string) throws Exception {
        sessionService.validate(session);
        return taskService.searchByString(string);
    }

}