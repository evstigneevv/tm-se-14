package ru.evstigneev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.api.endpoint.ISessionEndpoint;
import ru.evstigneev.tm.api.service.ISessionService;
import ru.evstigneev.tm.dto.SessionDto;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.evstigneev.tm.api.endpoint.ISessionEndpoint")
public class SessionEndpoint implements ISessionEndpoint {

    @NotNull
    private ISessionService sessionService;

    public SessionEndpoint() {
    }

    public SessionEndpoint(@NotNull final ISessionService sessionService) {
        this.sessionService = sessionService;
    }

    @Override
    @WebMethod
    public void validate(@WebParam(name = "session") @NotNull final SessionDto session) throws Exception {
        sessionService.validate(session);
    }

    @Override
    @WebMethod
    public SessionDto openSession(@WebParam(name = "login") @NotNull final String login,
                                  @WebParam(name = "password") @NotNull final String password) throws Exception {
        return sessionService.openSession(login, password);
    }

    @Override
    @WebMethod
    public boolean closeSession(@WebParam(name = "session") @NotNull final SessionDto session) throws Exception {
        sessionService.validate(session);
        return sessionService.closeSession(session);
    }

}
