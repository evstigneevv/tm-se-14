package ru.evstigneev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.api.endpoint.IProjectEndpoint;
import ru.evstigneev.tm.api.service.IProjectService;
import ru.evstigneev.tm.api.service.ISessionService;
import ru.evstigneev.tm.dto.ProjectDto;
import ru.evstigneev.tm.dto.SessionDto;
import ru.evstigneev.tm.enumerated.RoleType;
import ru.evstigneev.tm.enumerated.Status;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService(endpointInterface = "ru.evstigneev.tm.api.endpoint.IProjectEndpoint")
public class ProjectEndpoint implements IProjectEndpoint {

    @NotNull
    private IProjectService projectService;
    @NotNull
    private ISessionService sessionService;

    public ProjectEndpoint() {
    }

    public ProjectEndpoint(@NotNull final IProjectService projectService, @NotNull final ISessionService sessionService) {
        this.projectService = projectService;
        this.sessionService = sessionService;
    }

    @Override
    @WebMethod
    public Collection<ProjectDto> findAllProjects(@WebParam(name = "session") @Nullable final SessionDto session) throws Exception {
        sessionService.validate(session, RoleType.ADMIN);
        return projectService.findAll();
    }

    @Override
    @WebMethod
    public void createProject(@WebParam(name = "session") @Nullable final SessionDto session,
                              @WebParam(name = "projectName") @NotNull final String projectName,
                              @WebParam(name = "description") @NotNull final String description) throws Exception {
        sessionService.validate(session);
        projectService.create(session.getUserId(), projectName, description);
    }

    @Override
    @WebMethod
    public void removeProject(@WebParam(name = "session") @Nullable final SessionDto session,
                              @WebParam(name = "projectId") @NotNull final String projectId) throws Exception {
        sessionService.validate(session);
        projectService.remove(session.getUserId(), projectId);
    }

    @Override
    @WebMethod
    public void updateProject(@WebParam(name = "session") @Nullable final SessionDto session,
                              @WebParam(name = "projectId") @NotNull final String projectId,
                              @WebParam(name = "newProjectName") @NotNull final String newProjectName,
                              @WebParam(name = "description") @Nullable final String description,
                              @WebParam(name = "dateStart") @Nullable final String dateStart,
                              @WebParam(name = "dateFinish") @Nullable final String dateFinish,
                              @WebParam(name = "status") @NotNull final Status status) throws Exception {
        sessionService.validate(session);
        projectService.update(session.getUserId(), projectId, newProjectName, description, dateStart,
                dateFinish, status);
    }

    @Override
    @WebMethod
    public ProjectDto findOneProject(@WebParam(name = "session") @Nullable final SessionDto session,
                                     @WebParam(name = "projectId") @NotNull final String projectId) throws Exception {
        sessionService.validate(session);
        return projectService.findOne(session.getUserId(), projectId);
    }

    @Override
    @WebMethod
    public void mergeProject(@WebParam(name = "session") @Nullable final SessionDto session,
                             @WebParam(name = "project") @NotNull final ProjectDto project) throws Exception {
        sessionService.validate(session);
        projectService.merge(project);
    }

    @Override
    @WebMethod
    public void persistProject(@WebParam(name = "session") @Nullable final SessionDto session,
                               @WebParam(name = "project") @NotNull final ProjectDto project) throws Exception {
        sessionService.validate(session);
        projectService.persist(project);
    }

    @Override
    @WebMethod
    public Collection<ProjectDto> findAllProjectsByUserId(@WebParam(name = "session") @Nullable final SessionDto session) throws Exception {
        sessionService.validate(session);
        return projectService.findAllByUserId(session.getUserId());
    }

    @Override
    @WebMethod
    public void removeAllProjects(@WebParam(name = "session") @Nullable final SessionDto session) throws Exception {
        sessionService.validate(session, RoleType.ADMIN);
        projectService.removeAll();
    }

    @Override
    @WebMethod
    public void removeAllProjectsByUserId(@WebParam(name = "session") @Nullable final SessionDto session) throws Exception {
        sessionService.validate(session);
        projectService.removeAllByUserId(session.getUserId());
    }

    @Override
    @WebMethod
    public List<ProjectDto> sortProjects(@WebParam(name = "session") @Nullable final SessionDto session,
                                         @WebParam(name = "comparatorName") @NotNull final String comparatorName) throws Exception {
        sessionService.validate(session);
        return projectService.sort(comparatorName);
    }

    @Override
    @WebMethod
    public List<ProjectDto> searchProjectByString(@WebParam(name = "session") @Nullable final SessionDto session,
                                                  @WebParam(name = "string") @NotNull final String string) throws Exception {
        sessionService.validate(session);
        return projectService.searchByString(string);
    }

}
