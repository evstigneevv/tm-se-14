package ru.evstigneev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.dto.SessionDto;
import ru.evstigneev.tm.dto.UserDto;
import ru.evstigneev.tm.enumerated.RoleType;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService
public interface IUserEndpoint {

    String URL = "http://localhost:8080/UserEndpoint?wsdl";

    @WebMethod
    void createAdmin(@WebParam(name = "login") @NotNull final String login,
                     @WebParam(name = "password") @NotNull final String password) throws Exception;

    @WebMethod
    void createUser(@WebParam(name = "session") @NotNull final SessionDto session,
                    @WebParam(name = "login") @NotNull final String login,
                    @WebParam(name = "password") @NotNull final String password,
                    @WebParam(name = "role") @NotNull final RoleType role) throws Exception;

    @WebMethod
    Collection<UserDto> findAllUsers(@WebParam(name = "session") @NotNull final SessionDto session) throws Exception;

    @WebMethod
    boolean checkPassword(@WebParam(name = "login") @NotNull final String login,
                          @WebParam(name = "password") @NotNull final String password) throws Exception;

    @WebMethod
    boolean checkPasswordByUserId(@WebParam(name = "userId") @NotNull final String userId,
                                  @WebParam(name = "password") @NotNull final String password) throws Exception;

    @WebMethod
    void updatePassword(@WebParam(name = "session") @NotNull final SessionDto session,
                        @WebParam(name = "newPassword") @NotNull final String newPassword) throws Exception;

    @WebMethod
    UserDto findUserByLogin(@WebParam(name = "session") @NotNull final SessionDto session,
                            @WebParam(name = "login") @NotNull final String login) throws Exception;

    @WebMethod
    void updateUser(@WebParam(name = "session") @NotNull final SessionDto session,
                    @WebParam(name = "login") @NotNull final String login,
                    @WebParam(name = "password") @NotNull final String password,
                    @WebParam(name = "role") @NotNull final RoleType role) throws Exception;

    @WebMethod
    void removeUser(@WebParam(name = "session") @NotNull final SessionDto session,
                    @WebParam(name = "userId") @NotNull final String userId) throws Exception;

    @WebMethod
    void removeAllUsers(@WebParam(name = "session") @NotNull final SessionDto session) throws Exception;

    @WebMethod
    boolean isEmptyUserList() throws Exception;

}
