package ru.evstigneev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.dto.ProjectDto;
import ru.evstigneev.tm.dto.SessionDto;
import ru.evstigneev.tm.enumerated.Status;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
public interface IProjectEndpoint {

    String URL = "http://localhost:8080/ProjectEndpoint?wsdl";

    @WebMethod
    Collection<ProjectDto> findAllProjects(@WebParam(name = "session") @NotNull final SessionDto session) throws Exception;

    @WebMethod
    void createProject(@WebParam(name = "session") @NotNull final SessionDto session,
                       @WebParam(name = "projectName") @NotNull final String projectName,
                       @WebParam(name = "description") @NotNull final String description) throws Exception;

    @WebMethod
    void removeProject(@WebParam(name = "session") @NotNull final SessionDto session,
                       @WebParam(name = "projectId") @NotNull final String projectId) throws Exception;

    @WebMethod
    void updateProject(@WebParam(name = "session") @NotNull final SessionDto session,
                       @WebParam(name = "projectId") @NotNull final String projectId,
                       @WebParam(name = "newProjectName") @NotNull final String newProjectName,
                       @WebParam(name = "description") @Nullable final String description,
                       @WebParam(name = "dateStart") @Nullable final String dateStart,
                       @WebParam(name = "dateFinish") @Nullable final String dateFinish,
                       @WebParam(name = "status") @NotNull final Status status) throws Exception;

    @WebMethod
    ProjectDto findOneProject(@WebParam(name = "session") @NotNull final SessionDto session,
                              @WebParam(name = "projectId") @NotNull final String projectId) throws Exception;

    @WebMethod
    void mergeProject(@WebParam(name = "session") @NotNull final SessionDto session,
                      @WebParam(name = "project") @NotNull final ProjectDto project) throws Exception;

    @WebMethod
    void persistProject(@WebParam(name = "session") @NotNull final SessionDto session,
                        @WebParam(name = "project") @NotNull final ProjectDto project) throws Exception;

    @WebMethod
    Collection<ProjectDto> findAllProjectsByUserId(@WebParam(name = "session") @NotNull final SessionDto session) throws Exception;

    @WebMethod
    void removeAllProjects(@WebParam(name = "session") @NotNull final SessionDto session) throws Exception;

    @WebMethod
    void removeAllProjectsByUserId(@WebParam(name = "session") @NotNull final SessionDto session) throws Exception;

    @WebMethod
    List<ProjectDto> sortProjects(@WebParam(name = "session") @NotNull final SessionDto session,
                                  @WebParam(name = "comparatorName") @NotNull final String comparatorName) throws Exception;

    @WebMethod
    List<ProjectDto> searchProjectByString(@WebParam(name = "session") @NotNull final SessionDto session,
                                           @WebParam(name = "string") @NotNull final String string) throws Exception;

}
