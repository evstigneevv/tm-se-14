package ru.evstigneev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.dto.ProjectDto;
import ru.evstigneev.tm.enumerated.Status;

import java.util.Collection;
import java.util.List;

public interface IProjectService {

    Collection<ProjectDto> findAll() throws Exception;

    void create(@NotNull final String userId, @NotNull final String projectName,
                @NotNull final String description) throws Exception;

    void persist(@NotNull final ProjectDto project) throws Exception;

    void remove(@NotNull final String userId, @NotNull final String projectId) throws Exception;

    void update(@NotNull final String userId, @NotNull final String projectId,
                @NotNull final String name, @Nullable final String description,
                @Nullable final String dateStart, @Nullable final String dateFinish, @NotNull final Status status) throws Exception;

    ProjectDto findOne(@NotNull final String userId, @NotNull final String projectId) throws Exception;

    Collection<ProjectDto> findAllByUserId(@NotNull final String userId) throws Exception;

    void removeAll() throws Exception;

    void removeAllByUserId(@NotNull final String userId) throws Exception;

    List<ProjectDto> sort(@NotNull final String comparator) throws Exception;

    List<ProjectDto> searchByString(@NotNull final String string) throws Exception;

    void merge(ProjectDto project) throws Exception;

}
