package ru.evstigneev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.dto.UserDto;
import ru.evstigneev.tm.enumerated.RoleType;

import java.util.Collection;

public interface IUserService {

    Collection<UserDto> findAll() throws Exception;

    boolean checkPassword(@NotNull final String login, @NotNull final String password) throws Exception;

    boolean checkPasswordByUserId(@NotNull final String userId, @NotNull final String password) throws Exception;

    void updatePassword(@NotNull final String userId, @NotNull final String newPassword) throws Exception;

    UserDto findByLogin(@NotNull final String login) throws Exception;

    void update(@NotNull final String userId, @NotNull final String login, @NotNull final String password,
                @NotNull final RoleType role) throws Exception;

    void remove(@NotNull final String userId) throws Exception;

    void persist(@NotNull final String login, @NotNull final String password) throws Exception;

    void persist(@NotNull final UserDto user) throws Exception;

    void persist(@NotNull final String login, @NotNull final String password, @NotNull final RoleType role) throws Exception;

    void merge(@NotNull final UserDto user) throws Exception;

    void removeAll() throws Exception;

}
