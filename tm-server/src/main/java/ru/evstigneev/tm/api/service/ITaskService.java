package ru.evstigneev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.dto.TaskDto;
import ru.evstigneev.tm.enumerated.Status;

import java.util.Collection;
import java.util.List;

public interface ITaskService {

    void create(@NotNull final String userId, @NotNull final String projectId, @NotNull final String name,
                @NotNull final String description) throws Exception;

    Collection<TaskDto> findAll() throws Exception;

    Collection<TaskDto> findAllByUserId(@NotNull final String userId) throws Exception;

    void remove(@NotNull final String userId, @NotNull final String taskId) throws Exception;

    Collection<TaskDto> getTaskListByProjectId(@NotNull final String userId, @NotNull final String projectId) throws Exception;

    void update(@NotNull final String userId, @NotNull final String taskId,
                @NotNull final String name, @Nullable final String description,
                @Nullable final String dateStart, @Nullable final String dateFinish,
                @NotNull final Status status) throws Exception;

    TaskDto findOne(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskId) throws Exception;

    void merge(@NotNull final String userId, @NotNull final TaskDto taskDto) throws Exception;

    void persist(@NotNull final String userId, @NotNull final TaskDto taskDto) throws Exception;

    void removeAll() throws Exception;

    void removeAllByUserId(@NotNull final String userId) throws Exception;

    void deleteAllProjectTasks(@NotNull final String userId, @NotNull final String projectId) throws Exception;

    List<TaskDto> sort(@NotNull final String comparator) throws Exception;

    List<TaskDto> searchByString(@NotNull final String string) throws Exception;

}
