package ru.evstigneev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.dto.SessionDto;
import ru.evstigneev.tm.enumerated.RoleType;

public interface ISessionService {

    void validate(@Nullable final SessionDto session) throws Exception;

    void validate(@Nullable final SessionDto session, @NotNull final RoleType roleType) throws Exception;

    SessionDto openSession(@NotNull final String login, @NotNull final String password) throws Exception;

    boolean closeSession(@NotNull final SessionDto session) throws Exception;

}
