package ru.evstigneev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.dto.SessionDto;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface ISessionEndpoint {

    String URL = "http://localhost:8080/SessionEndpoint?wsdl";

    @WebMethod
    void validate(@WebParam(name = "session") @NotNull final SessionDto session) throws Exception;

    @WebMethod
    SessionDto openSession(@WebParam(name = "login") @NotNull final String login,
                           @WebParam(name = "password") @NotNull final String password) throws Exception;

    @WebMethod
    boolean closeSession(@WebParam(name = "session") @NotNull final SessionDto session) throws Exception;

}
