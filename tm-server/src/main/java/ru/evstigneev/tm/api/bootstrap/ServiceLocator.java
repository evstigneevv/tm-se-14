package ru.evstigneev.tm.api.bootstrap;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.api.endpoint.*;
import ru.evstigneev.tm.api.service.IProjectService;
import ru.evstigneev.tm.api.service.ITaskService;
import ru.evstigneev.tm.api.service.IUserService;
import ru.evstigneev.tm.util.DateParser;

import java.util.Scanner;

public interface ServiceLocator {

    Scanner getScanner();

    IProjectService getProjectService();

    ITaskService getTaskService();

    IUserService getUserService();

    IUserEndpoint getUserEndpoint();

    void setUserEndpoint(@NotNull final IUserEndpoint userEndpoint);

    IProjectEndpoint getProjectEndpoint();

    void setProjectEndpoint(@NotNull final IProjectEndpoint projectEndpoint);

    ITaskEndpoint getTaskEndpoint();

    void setTaskEndpoint(@NotNull final ITaskEndpoint taskEndpoint);

    DateParser getDateParser();

    ISessionEndpoint getSessionEndpoint();

    void setSessionEndpoint(@NotNull final ISessionEndpoint sessionEndpoint);

    IDomainEndpoint getDomainEndpoint();

    void setDomainEndpoint(@NotNull final IDomainEndpoint domainEndpoint);

    void init();

}
