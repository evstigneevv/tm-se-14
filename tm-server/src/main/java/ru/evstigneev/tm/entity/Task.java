package ru.evstigneev.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "tasks")
@NoArgsConstructor
public class Task implements Comparable<Task> {

    @Nullable
    @Column(name = "date_of_creation")
    private Date dateOfCreation;

    @NotNull
    private String name;

    @Id
    @NotNull
    private String id;

    @Nullable
    private String description;

    @Nullable
    @Column(name = "date_start")
    private Date dateStart;

    @Nullable
    @Column(name = "date_finish")
    private Date dateFinish;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project_id")
    private Project project;

    @Enumerated(EnumType.STRING)
    @NotNull
    private Status status;

    @Override
    public int compareTo(@NotNull final Task task) {
        return this.getStatus().compareTo(task.getStatus());
    }

}
