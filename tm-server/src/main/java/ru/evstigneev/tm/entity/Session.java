package ru.evstigneev.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.enumerated.RoleType;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "sessions")
@NoArgsConstructor
public class Session implements Cloneable {

    @Id
    @NotNull
    private String id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    @NotNull
    private User user;

    @Column(name = "role_type")
    @NotNull
    @Enumerated(EnumType.STRING)
    private RoleType roleType;

    @NotNull
    private Long timestamp;

    @Nullable
    private String signature;

    @Override
    public Session clone() throws CloneNotSupportedException {
        return (Session) super.clone();
    }

}
