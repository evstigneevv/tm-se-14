package ru.evstigneev.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "projects")
@NoArgsConstructor
public class Project implements Comparable<Project> {

    @Column(name = "date_of_creation")
    @Nullable
    private Date dateOfCreation;

    @NotNull
    private String name;

    @Id
    @NotNull
    private String id;

    @Nullable
    private String description;

    @Column(name = "date_start")
    @Nullable
    private Date dateStart;

    @Column(name = "date_finish")
    @Nullable
    private Date dateFinish;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @Enumerated(EnumType.STRING)
    @NotNull
    private Status status;

    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "project", cascade = CascadeType.REMOVE)
    @Nullable
    private List<Task> tasks;

    @Override
    public int compareTo(@NotNull final Project project) {
        return this.getStatus().compareTo(project.getStatus());
    }

}
