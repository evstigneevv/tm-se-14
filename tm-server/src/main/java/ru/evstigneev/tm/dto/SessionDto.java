package ru.evstigneev.tm.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.enumerated.RoleType;

@Getter
@Setter
@ToString
public class SessionDto implements Cloneable {

    @NotNull
    private String id;
    @NotNull
    private String UserId;
    @NotNull
    private RoleType roleType;
    @NotNull
    private Long timestamp;
    @Nullable
    private String signature;

    @Override
    public SessionDto clone() throws CloneNotSupportedException {
        return (SessionDto) super.clone();
    }

}
