package ru.evstigneev.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.enumerated.RoleType;

import java.io.Serializable;

@NoArgsConstructor
@Getter
@Setter
public class UserDto implements Serializable {

    @NotNull
    private String login;
    @NotNull
    private String id;
    @NotNull
    private String password;
    @NotNull
    private RoleType role;

    @Override
    public String toString() {
        return "User ID: " + getId() + " | Login:" + getLogin() + " | role: " + role.displayName();
    }

}
