package ru.evstigneev.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.enumerated.Status;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class TaskDto implements Comparable<TaskDto>, Serializable {

    @Nullable
    private Date dateOfCreation;
    @NotNull
    private String name;
    @NotNull
    private String id;
    @NotNull
    private String projectId;
    @Nullable
    private String description;
    @Nullable
    private Date dateStart;
    @Nullable
    private Date dateFinish;
    @NotNull
    private String userId;
    @NotNull
    private Status status;

    @Override
    public String toString() {
        return "User ID: " + getUserId() + " | Project ID:" + getProjectId() + " | Task ID:" + getId() + " | Task name: "
                + getName() + " | Task description: " + getDescription() + " | Date of creation: "
                + getDateOfCreation() + " | Date of start: " + getDateStart() + " | Date of finish: "
                + getDateFinish() + " | Task status: " + getStatus();
    }

    @Override
    public int compareTo(@NotNull final TaskDto taskDto) {
        return this.getStatus().compareTo(taskDto.getStatus());
    }

}

