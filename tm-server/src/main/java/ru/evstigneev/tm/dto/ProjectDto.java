package ru.evstigneev.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.enumerated.Status;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class ProjectDto implements Comparable<ProjectDto>, Serializable {

    @Nullable
    private Date dateOfCreation;
    @NotNull
    private String name;
    @NotNull
    private String id;
    @Nullable
    private String description;
    @Nullable
    private Date dateStart;
    @Nullable
    private Date dateFinish;
    @NotNull
    private String userId;
    @NotNull
    private Status status;

    @Override
    public String toString() {
        return "User ID: " + getUserId() + " | Project ID:" + getId() + " | Project name: "
                + getName() + " | Project description: " + getDescription() + " | Date of creation: "
                + getDateOfCreation() + " | Date of start: " + getDateStart() + " | Date of finish: "
                + getDateFinish() + " | Project status: " + getStatus();
    }

    @Override
    public int compareTo(@NotNull final ProjectDto projectDto) {
        return this.getStatus().compareTo(projectDto.getStatus());
    }

}
