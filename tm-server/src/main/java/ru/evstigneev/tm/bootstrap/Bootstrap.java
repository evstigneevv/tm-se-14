package ru.evstigneev.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.api.bootstrap.ServiceLocator;
import ru.evstigneev.tm.api.endpoint.*;
import ru.evstigneev.tm.api.service.IProjectService;
import ru.evstigneev.tm.api.service.ISessionService;
import ru.evstigneev.tm.api.service.ITaskService;
import ru.evstigneev.tm.api.service.IUserService;
import ru.evstigneev.tm.endpoint.*;
import ru.evstigneev.tm.service.ProjectService;
import ru.evstigneev.tm.service.SessionService;
import ru.evstigneev.tm.service.TaskService;
import ru.evstigneev.tm.service.UserService;
import ru.evstigneev.tm.util.DateParser;
import ru.evstigneev.tm.util.HibernateUtil;

import javax.persistence.EntityManagerFactory;
import javax.xml.ws.Endpoint;
import java.util.Scanner;

public class Bootstrap implements ServiceLocator {

    @NotNull
    private final EntityManagerFactory entityManagerFactory = new HibernateUtil().factory();
    @NotNull
    private final Scanner scanner = new Scanner(System.in);
    @NotNull
    private final IProjectService projectService = new ProjectService(entityManagerFactory);
    @NotNull
    private final ITaskService taskService = new TaskService(entityManagerFactory);
    @NotNull
    private final IUserService userService = new UserService(entityManagerFactory);
    @NotNull
    private final ISessionService sessionService = new SessionService(entityManagerFactory);
    @NotNull
    private final DateParser dateParser = new DateParser();
    @NotNull
    private IUserEndpoint userEndpoint = new UserEndpoint(userService, sessionService);
    @NotNull
    private IProjectEndpoint projectEndpoint = new ProjectEndpoint(projectService, sessionService);
    @NotNull
    private ITaskEndpoint taskEndpoint = new TaskEndpoint(taskService, projectService, sessionService);
    @NotNull
    private ISessionEndpoint sessionEndpoint = new SessionEndpoint(sessionService);
    @NotNull
    private IDomainEndpoint domainEndpoint = new DomainEndpoint(userService, projectService, taskService, sessionService);

    public void init() {
        System.out.println(Endpoint.publish(IUserEndpoint.URL, userEndpoint));
        System.out.println(Endpoint.publish(IProjectEndpoint.URL, projectEndpoint));
        System.out.println(Endpoint.publish(ITaskEndpoint.URL, taskEndpoint));
        System.out.println(Endpoint.publish(ISessionEndpoint.URL, sessionEndpoint));
        System.out.println(Endpoint.publish(IDomainEndpoint.URL, domainEndpoint));
    }

    @NotNull
    @Override
    public Scanner getScanner() {
        return scanner;
    }

    @NotNull
    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    public IUserService getUserService() {
        return userService;
    }

    @NotNull
    @Override
    public IUserEndpoint getUserEndpoint() {
        return userEndpoint;
    }

    @Override
    public void setUserEndpoint(@NotNull final IUserEndpoint userEndpoint) {
        this.userEndpoint = userEndpoint;
    }

    @NotNull
    @Override
    public IProjectEndpoint getProjectEndpoint() {
        return projectEndpoint;
    }

    @Override
    public void setProjectEndpoint(@NotNull final IProjectEndpoint projectEndpoint) {
        this.projectEndpoint = projectEndpoint;
    }

    @NotNull
    @Override
    public ITaskEndpoint getTaskEndpoint() {
        return taskEndpoint;
    }

    @Override
    public void setTaskEndpoint(@NotNull final ITaskEndpoint taskEndpoint) {
        this.taskEndpoint = taskEndpoint;
    }

    @NotNull
    @Override
    public DateParser getDateParser() {
        return dateParser;
    }

    @NotNull
    @Override
    public ISessionEndpoint getSessionEndpoint() {
        return sessionEndpoint;
    }

    public @Override
    void setSessionEndpoint(@NotNull final ISessionEndpoint sessionEndpoint) {
        this.sessionEndpoint = sessionEndpoint;
    }

    @NotNull
    @Override
    public IDomainEndpoint getDomainEndpoint() {
        return domainEndpoint;
    }

    @Override
    public void setDomainEndpoint(@NotNull final IDomainEndpoint domainEndpoint) {
        this.domainEndpoint = domainEndpoint;
    }

    @NotNull
    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

}
