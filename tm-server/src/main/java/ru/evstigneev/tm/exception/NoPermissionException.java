package ru.evstigneev.tm.exception;

public class NoPermissionException extends Exception {

    public NoPermissionException() {
        super("You don't have permission for that command!");
    }

    public NoPermissionException(String message) {
        super(message);
    }

}
