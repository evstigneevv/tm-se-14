package ru.evstigneev.tm.enumerated;

import org.jetbrains.annotations.NotNull;

public enum RoleType {

    ADMIN("ADMIN"),
    USER("USER");

    private String role;

    RoleType(@NotNull final String role) {
        this.role = role;
    }

    public String displayName() {
        return role;
    }
}
