package ru.evstigneev.tm.comparator;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.dto.TaskDto;

import java.util.Comparator;

public class ComparatorTaskDateCreation implements Comparator<TaskDto> {

    @Override
    public int compare(@NotNull final TaskDto o1, @NotNull final TaskDto o2) {
        return o1.getDateOfCreation().compareTo(o2.getDateOfCreation());
    }

}
