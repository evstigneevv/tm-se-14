package ru.evstigneev.tm.comparator;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.dto.ProjectDto;

import java.util.Comparator;

public class ComparatorProjectDateCreation implements Comparator<ProjectDto> {

    @Override
    public int compare(@NotNull final ProjectDto o1, @NotNull final ProjectDto o2) {
        return o1.getDateOfCreation().compareTo(o2.getDateOfCreation());
    }

}
