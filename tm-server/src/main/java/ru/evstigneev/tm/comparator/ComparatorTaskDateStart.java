package ru.evstigneev.tm.comparator;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.dto.TaskDto;

import java.util.Comparator;

public class ComparatorTaskDateStart implements Comparator<TaskDto> {

    @Override
    public int compare(@NotNull final TaskDto o1, @NotNull final TaskDto o2) {
        if (o1.getDateStart() != null && o2.getDateStart() == null) {
            return -1;
        }
        if (o1.getDateStart() == null && o2.getDateStart() != null) {
            return 1;
        }
        if (o1.getDateStart() == null || o2.getDateStart() == null) {
            return 0;
        }
        return o1.getDateStart().compareTo(o2.getDateStart());
    }

}
