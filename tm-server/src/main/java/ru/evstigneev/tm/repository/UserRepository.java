package ru.evstigneev.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.entity.User;
import ru.evstigneev.tm.enumerated.RoleType;

import javax.persistence.EntityManager;
import java.util.List;

public class UserRepository {

    @NotNull
    private final EntityManager entityManager;

    @NotNull
    public UserRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void updatePassword(@NotNull final String userId,
                               @NotNull final String newPassword) {
        @NotNull final String query = "UPDATE User user SET user.password = :password WHERE user.id = :userId";
        entityManager.createQuery(query).setParameter("userId", userId).setParameter("password", newPassword).executeUpdate();
    }

    public User findByLogin(@NotNull final String login) {
        @NotNull final String query = "FROM User user WHERE login = :login";
        return entityManager.createQuery(query, User.class).setParameter("login", login).getSingleResult();
    }

    public User findByUserId(@NotNull final String userId) {
        @NotNull final String query = "FROM User user WHERE user.id = :userId";
        return entityManager.createQuery(query, User.class).setParameter("userId", userId).getSingleResult();
    }

    public List<User> findAll() {
        return entityManager.createQuery("FROM User user").getResultList();
    }

    public void update(@NotNull final String userId, @NotNull final String login, @NotNull final String password,
                       @NotNull final RoleType role) {
        @NotNull final String query = "UPDATE User user SET user.login = :login, user.password = :password, user" +
                ".role = :role WHERE user.id = :userId";
        entityManager.createQuery(query).setParameter("userId", userId).setParameter("login", login).setParameter(
                "password", password).setParameter("role", role).executeUpdate();
    }

    public void remove(@NotNull final String userId) {
        @NotNull final String query = "DELETE FROM User user WHERE user.id = :userId";
        entityManager.createQuery(query).setParameter("userId", userId).executeUpdate();
    }

    public void persist(@NotNull final User user) {
        entityManager.persist(user);
    }

    public void removeAll() {
        entityManager.createQuery("DELETE FROM user").executeUpdate();
    }

    public void merge(@NotNull final User user) {
        entityManager.merge(user);
    }

}
