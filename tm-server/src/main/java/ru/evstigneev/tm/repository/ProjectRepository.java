package ru.evstigneev.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.entity.Project;

import javax.persistence.EntityManager;
import java.util.Collection;

public class ProjectRepository {

    @NotNull
    private final EntityManager entityManager;

    public ProjectRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void remove(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final String query = "DELETE FROM Project project WHERE project.user.id = :userId AND id = :projectId";
        entityManager.createQuery(query).setParameter("userId", userId).setParameter("projectId", projectId).executeUpdate();
    }

    public void update(@NotNull final Project project) {
        @NotNull final String query = "UPDATE Project project SET project.name = :name, project.description = " +
                ":description, project.dateStart = :dateStart, project.dateFinish = :dateFinish, project.status =: " +
                "status where project.user.id = :userId and project.id = : projectId";
        entityManager.createQuery(query).setParameter("userId", project.getUser().getId()).setParameter("projectId",
                project.getId()).setParameter("name", project.getName()).setParameter("description",
                project.getDescription()).setParameter("dateStart", project.getDateStart()).setParameter("dateFinish"
                , project.getDateFinish()).setParameter("status", project.getStatus()).executeUpdate();
    }

    public void merge(@NotNull final Project project) {
        entityManager.merge(project);
    }

    public Collection<Project> findAll() {
        return entityManager.createQuery("FROM Project project").getResultList();
    }

    public Collection<Project> findAllByUserId(@NotNull final String userId) {
        return entityManager.createQuery("FROM Project project where project.user.id =: userId").setParameter(
                "userId", userId).getResultList();
    }

    public Project findOne(@NotNull final String userId, @NotNull final String projectId) {
        return (Project) entityManager.createQuery("FROM Project project where project.user.id = :userId and project" +
                ".id = :projectId").setParameter("userId", userId).setParameter("projectId", projectId).getSingleResult();
    }

    public void persist(@NotNull final Project project) {
        entityManager.persist(project);
    }

    public void removeAllByUserId(@NotNull final String userId) {
        @NotNull final String query = "DELETE FROM Project project WHERE project.user.id = :userId";
        entityManager.createQuery(query).setParameter("userId", userId).executeUpdate();
    }

    public void removeAll() {
        @NotNull final String query = "DELETE FROM Project project";
        entityManager.createQuery(query).executeUpdate();
    }

}
