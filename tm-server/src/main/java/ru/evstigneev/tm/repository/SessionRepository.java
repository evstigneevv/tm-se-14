package ru.evstigneev.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.entity.Session;

import javax.persistence.EntityManager;

public class SessionRepository {

    @NotNull
    private final EntityManager entityManager;

    public SessionRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void persist(@NotNull final Session session) {
        entityManager.persist(session);
    }

    public void remove(@NotNull final Session session) {
        @NotNull final String query = "DELETE FROM Session session WHERE session.id = :id";
        entityManager.createQuery(query).setParameter("id", session.getId()).executeUpdate();
    }

}
