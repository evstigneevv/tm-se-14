package ru.evstigneev.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.entity.Task;

import javax.persistence.EntityManager;
import java.util.Collection;

public class TaskRepository {

    @NotNull
    private final EntityManager entityManager;

    public TaskRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void remove(@NotNull final String userId, @NotNull final String taskId) {
        @NotNull final String query = "DELETE FROM Task task WHERE task.user.id = :userId AND id = :taskId";
        entityManager.createQuery(query).setParameter("userId", userId).setParameter("taskId", taskId).executeUpdate();
    }

    public void update(@NotNull final Task task) {
        @NotNull final String query = "UPDATE Task task SET task.name = :name, task.description = " +
                ":description, task.dateStart = :dateStart, task.dateFinish = :dateFinish, task.status =: " +
                "status where task.user.id = :userId and task.id = : taskId";
        entityManager.createQuery(query).setParameter("userId", task.getUser().getId()).setParameter("taskId",
                task.getId()).setParameter("name", task.getName()).setParameter("description",
                task.getDescription()).setParameter("dateStart", task.getDateStart()).setParameter("dateFinish"
                , task.getDateFinish()).setParameter("status", task.getStatus()).executeUpdate();
    }

    public Collection<Task> getTaskListByProjectId(@NotNull final String userId,
                                                   @NotNull final String projectId) {
        return entityManager.createQuery("FROM Task task where task.user.id = :userId and task.project.id = " +
                ":projectId").setParameter("userId", userId).setParameter("projectId", projectId).getResultList();
    }

    public Collection<Task> findAll() {
        return entityManager.createQuery("FROM Task task").getResultList();
    }

    public Collection<Task> findAllByUserId(@NotNull final String userId) {
        return entityManager.createQuery("FROM Task task where task.user.id =: userId").setParameter(
                "userId", userId).getResultList();
    }

    public void deleteAllProjectTasks(@NotNull final String userId,
                                      @NotNull final String projectId) {
        @NotNull final String query = "DELETE FROM Task task WHERE task.user.id = :userId AND task.project.id = " +
                ":projectId";
        entityManager.createQuery(query).setParameter("userId", userId).setParameter("projectId", projectId).executeUpdate();
    }

    public Task findOne(@NotNull final String userId, @NotNull final String taskId) {
        return (Task) entityManager.createQuery("FROM Task task where task.user.id = :userId and task.id = :taskId").setParameter("userId", userId)
                .setParameter("taskId", taskId).getSingleResult();
    }

    public void removeAll() {
        @NotNull final String query = "DELETE FROM Task task";
        entityManager.createQuery(query).executeUpdate();
    }

    public void removeAllByUserId(@NotNull final String userId) {
        @NotNull final String query = "DELETE FROM Task task WHERE task.user.id = :userId";
        entityManager.createQuery(query).setParameter("userId", userId).executeUpdate();
    }

    public void persist(@NotNull final Task task) {
        entityManager.persist(task);
    }

    public void merge(@NotNull final Task task) {
        entityManager.merge(task);
    }

}
