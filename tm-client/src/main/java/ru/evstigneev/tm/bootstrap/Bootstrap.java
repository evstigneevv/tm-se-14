package ru.evstigneev.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.evstigneev.tm.api.bootstrap.ServiceLocator;
import ru.evstigneev.tm.api.endpoint.*;
import ru.evstigneev.tm.command.AbstractCommand;
import ru.evstigneev.tm.endpoint.*;

import java.lang.Exception;
import java.util.*;

public class Bootstrap implements ServiceLocator {

    @NotNull
    private final Scanner scanner = new Scanner(System.in);
    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();
    @NotNull
    private final Set<Class<? extends AbstractCommand>> classes =
            new Reflections("ru.evstigneev.tm").getSubTypesOf(AbstractCommand.class);
    @NotNull
    private IUserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();
    @NotNull
    private IProjectEndpoint projectEndpoint = new ProjectEndpointService().getProjectEndpointPort();
    @NotNull
    private ITaskEndpoint taskEndpoint = new TaskEndpointService().getTaskEndpointPort();
    @NotNull
    private ISessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();
    @NotNull
    private IDomainEndpoint domainEndpoint = new DomainEndpointService().getDomainEndpointPort();
    @Nullable
    private SessionDto currentSession;

    public void init() {
        for (Class clazz : classes) {
            try {
                registry(clazz);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        start();
    }

    private void start() {
        System.out.println("Welcome to Task Manager!");
        System.out.print("input command or \"EXIT\" to exit...");
        while (true) {
            @NotNull final String command = scanner.nextLine().toUpperCase();
            try {
                execute(command);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void registry(@NotNull final Class clazz) throws Exception {
        if (clazz.getSuperclass().equals(AbstractCommand.class)) {
            @NotNull final AbstractCommand command = (AbstractCommand) clazz.newInstance();
            command.setBootstrap(this);
            registryCommand(command);
        }
    }

    private void registryCommand(@NotNull final AbstractCommand command) throws Exception {
        @NotNull final String commandText = command.command();
        @NotNull final String description = command.description();
        if (commandText.isEmpty() || description.isEmpty())
            throw new Exception();
        commands.put(commandText, command);
    }

    private void execute(@NotNull final String command) throws Exception {
        if (command.isEmpty()) {
            throw new IllegalArgumentException();
        }
        @NotNull final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) {
            throw new Exception("Command corrupted!");
        }
        abstractCommand.execute();
    }

    @NotNull
    public IUserEndpoint getUserEndpoint() {
        return userEndpoint;
    }

    @Override
    public void setUserEndpoint(@NotNull final IUserEndpoint userEndpoint) {
        this.userEndpoint = userEndpoint;
    }

    @NotNull
    @Override
    public IProjectEndpoint getProjectEndpoint() {
        return projectEndpoint;
    }

    @Override
    public void setProjectEndpoint(@NotNull final IProjectEndpoint projectEndpoint) {
        this.projectEndpoint = projectEndpoint;
    }

    @NotNull
    @Override
    public ITaskEndpoint getTaskEndpoint() {
        return taskEndpoint;
    }

    @Override
    public void setTaskEndpoint(@NotNull final ITaskEndpoint taskEndpoint) {
        this.taskEndpoint = taskEndpoint;
    }

    @Override
    public ISessionEndpoint getSessionEndpoint() {
        return sessionEndpoint;
    }

    @Override
    public void setSessionEndpoint(@NotNull final ISessionEndpoint sessionEndpoint) {
        this.sessionEndpoint = sessionEndpoint;
    }

    @NotNull
    public Scanner getScanner() {
        return scanner;
    }

    @Override
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    @NotNull
    public Set<Class<? extends AbstractCommand>> getClasses() {
        return classes;
    }

    @Override
    public SessionDto getSession() {
        return currentSession;
    }

    @Override
    public void setSession(@Nullable final SessionDto currentSession) {
        this.currentSession = currentSession;
    }

    public IDomainEndpoint getDomainEndpoint() {
        return domainEndpoint;
    }

    public void setDomainEndpoint(IDomainEndpoint domainEndpoint) {
        this.domainEndpoint = domainEndpoint;
    }

}
