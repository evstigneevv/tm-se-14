package ru.evstigneev.tm.api.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.FaultAction;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.2.7
 * 2020-02-10T19:00:36.774+03:00
 * Generated source version: 3.2.7
 *
 */
@WebService(targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", name = "ITaskEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface ITaskEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.api.tm.evstigneev.ru/ITaskEndpoint/findAllTasksRequest", output = "http://endpoint.api.tm.evstigneev.ru/ITaskEndpoint/findAllTasksResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.tm.evstigneev.ru/ITaskEndpoint/findAllTasks/Fault/Exception")})
    @RequestWrapper(localName = "findAllTasks", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.FindAllTasks")
    @ResponseWrapper(localName = "findAllTasksResponse", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.FindAllTasksResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ru.evstigneev.tm.api.endpoint.TaskDto> findAllTasks(
        @WebParam(name = "session", targetNamespace = "")
        ru.evstigneev.tm.api.endpoint.SessionDto session
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.tm.evstigneev.ru/ITaskEndpoint/mergeTaskRequest", output = "http://endpoint.api.tm.evstigneev.ru/ITaskEndpoint/mergeTaskResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.tm.evstigneev.ru/ITaskEndpoint/mergeTask/Fault/Exception")})
    @RequestWrapper(localName = "mergeTask", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.MergeTask")
    @ResponseWrapper(localName = "mergeTaskResponse", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.MergeTaskResponse")
    public void mergeTask(
        @WebParam(name = "session", targetNamespace = "")
        ru.evstigneev.tm.api.endpoint.SessionDto session,
        @WebParam(name = "task", targetNamespace = "")
        ru.evstigneev.tm.api.endpoint.TaskDto task
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.tm.evstigneev.ru/ITaskEndpoint/persistTaskRequest", output = "http://endpoint.api.tm.evstigneev.ru/ITaskEndpoint/persistTaskResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.tm.evstigneev.ru/ITaskEndpoint/persistTask/Fault/Exception")})
    @RequestWrapper(localName = "persistTask", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.PersistTask")
    @ResponseWrapper(localName = "persistTaskResponse", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.PersistTaskResponse")
    public void persistTask(
        @WebParam(name = "session", targetNamespace = "")
        ru.evstigneev.tm.api.endpoint.SessionDto session,
        @WebParam(name = "task", targetNamespace = "")
        ru.evstigneev.tm.api.endpoint.TaskDto task
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.tm.evstigneev.ru/ITaskEndpoint/removeAllTasksByUserIdRequest", output = "http://endpoint.api.tm.evstigneev.ru/ITaskEndpoint/removeAllTasksByUserIdResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.tm.evstigneev.ru/ITaskEndpoint/removeAllTasksByUserId/Fault/Exception")})
    @RequestWrapper(localName = "removeAllTasksByUserId", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.RemoveAllTasksByUserId")
    @ResponseWrapper(localName = "removeAllTasksByUserIdResponse", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.RemoveAllTasksByUserIdResponse")
    public void removeAllTasksByUserId(
        @WebParam(name = "session", targetNamespace = "")
        ru.evstigneev.tm.api.endpoint.SessionDto session
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.tm.evstigneev.ru/ITaskEndpoint/findOneRequest", output = "http://endpoint.api.tm.evstigneev.ru/ITaskEndpoint/findOneResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.tm.evstigneev.ru/ITaskEndpoint/findOne/Fault/Exception")})
    @RequestWrapper(localName = "findOne", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.FindOne")
    @ResponseWrapper(localName = "findOneResponse", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.FindOneResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.evstigneev.tm.api.endpoint.TaskDto findOne(
        @WebParam(name = "session", targetNamespace = "")
        ru.evstigneev.tm.api.endpoint.SessionDto session,
        @WebParam(name = "projectId", targetNamespace = "")
        java.lang.String projectId,
        @WebParam(name = "taskId", targetNamespace = "")
        java.lang.String taskId
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.tm.evstigneev.ru/ITaskEndpoint/deleteAllProjectTasksRequest", output = "http://endpoint.api.tm.evstigneev.ru/ITaskEndpoint/deleteAllProjectTasksResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.tm.evstigneev.ru/ITaskEndpoint/deleteAllProjectTasks/Fault/Exception")})
    @RequestWrapper(localName = "deleteAllProjectTasks", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.DeleteAllProjectTasks")
    @ResponseWrapper(localName = "deleteAllProjectTasksResponse", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.DeleteAllProjectTasksResponse")
    public void deleteAllProjectTasks(
        @WebParam(name = "session", targetNamespace = "")
        ru.evstigneev.tm.api.endpoint.SessionDto session,
        @WebParam(name = "projectId", targetNamespace = "")
        java.lang.String projectId
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.tm.evstigneev.ru/ITaskEndpoint/updateTaskRequest", output = "http://endpoint.api.tm.evstigneev.ru/ITaskEndpoint/updateTaskResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.tm.evstigneev.ru/ITaskEndpoint/updateTask/Fault/Exception")})
    @RequestWrapper(localName = "updateTask", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.UpdateTask")
    @ResponseWrapper(localName = "updateTaskResponse", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.UpdateTaskResponse")
    public void updateTask(
        @WebParam(name = "session", targetNamespace = "")
        ru.evstigneev.tm.api.endpoint.SessionDto session,
        @WebParam(name = "taskId", targetNamespace = "")
        java.lang.String taskId,
        @WebParam(name = "taskName", targetNamespace = "")
        java.lang.String taskName,
        @WebParam(name = "description", targetNamespace = "")
        java.lang.String description,
        @WebParam(name = "dateStart", targetNamespace = "")
        java.lang.String dateStart,
        @WebParam(name = "dateFinish", targetNamespace = "")
        java.lang.String dateFinish,
        @WebParam(name = "status", targetNamespace = "")
        ru.evstigneev.tm.api.endpoint.Status status
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.tm.evstigneev.ru/ITaskEndpoint/removeAllTasksRequest", output = "http://endpoint.api.tm.evstigneev.ru/ITaskEndpoint/removeAllTasksResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.tm.evstigneev.ru/ITaskEndpoint/removeAllTasks/Fault/Exception")})
    @RequestWrapper(localName = "removeAllTasks", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.RemoveAllTasks")
    @ResponseWrapper(localName = "removeAllTasksResponse", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.RemoveAllTasksResponse")
    public void removeAllTasks(
        @WebParam(name = "session", targetNamespace = "")
        ru.evstigneev.tm.api.endpoint.SessionDto session
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.tm.evstigneev.ru/ITaskEndpoint/removeTaskRequest", output = "http://endpoint.api.tm.evstigneev.ru/ITaskEndpoint/removeTaskResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.tm.evstigneev.ru/ITaskEndpoint/removeTask/Fault/Exception")})
    @RequestWrapper(localName = "removeTask", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.RemoveTask")
    @ResponseWrapper(localName = "removeTaskResponse", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.RemoveTaskResponse")
    public void removeTask(
        @WebParam(name = "session", targetNamespace = "")
        ru.evstigneev.tm.api.endpoint.SessionDto session,
        @WebParam(name = "taskId", targetNamespace = "")
        java.lang.String taskId
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.tm.evstigneev.ru/ITaskEndpoint/getTaskListByProjectIdRequest", output = "http://endpoint.api.tm.evstigneev.ru/ITaskEndpoint/getTaskListByProjectIdResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.tm.evstigneev.ru/ITaskEndpoint/getTaskListByProjectId/Fault/Exception")})
    @RequestWrapper(localName = "getTaskListByProjectId", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.GetTaskListByProjectId")
    @ResponseWrapper(localName = "getTaskListByProjectIdResponse", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.GetTaskListByProjectIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ru.evstigneev.tm.api.endpoint.TaskDto> getTaskListByProjectId(
        @WebParam(name = "session", targetNamespace = "")
        ru.evstigneev.tm.api.endpoint.SessionDto session,
        @WebParam(name = "projectId", targetNamespace = "")
        java.lang.String projectId
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.tm.evstigneev.ru/ITaskEndpoint/sortTasksRequest", output = "http://endpoint.api.tm.evstigneev.ru/ITaskEndpoint/sortTasksResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.tm.evstigneev.ru/ITaskEndpoint/sortTasks/Fault/Exception")})
    @RequestWrapper(localName = "sortTasks", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.SortTasks")
    @ResponseWrapper(localName = "sortTasksResponse", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.SortTasksResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ru.evstigneev.tm.api.endpoint.TaskDto> sortTasks(
        @WebParam(name = "session", targetNamespace = "")
        ru.evstigneev.tm.api.endpoint.SessionDto session,
        @WebParam(name = "comparatorName", targetNamespace = "")
        java.lang.String comparatorName
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.tm.evstigneev.ru/ITaskEndpoint/createTaskRequest", output = "http://endpoint.api.tm.evstigneev.ru/ITaskEndpoint/createTaskResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.tm.evstigneev.ru/ITaskEndpoint/createTask/Fault/Exception")})
    @RequestWrapper(localName = "createTask", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.CreateTask")
    @ResponseWrapper(localName = "createTaskResponse", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.CreateTaskResponse")
    public void createTask(
        @WebParam(name = "session", targetNamespace = "")
        ru.evstigneev.tm.api.endpoint.SessionDto session,
        @WebParam(name = "projectId", targetNamespace = "")
        java.lang.String projectId,
        @WebParam(name = "taskName", targetNamespace = "")
        java.lang.String taskName,
        @WebParam(name = "description", targetNamespace = "")
        java.lang.String description
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.tm.evstigneev.ru/ITaskEndpoint/searchTaskByStringRequest", output = "http://endpoint.api.tm.evstigneev.ru/ITaskEndpoint/searchTaskByStringResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.tm.evstigneev.ru/ITaskEndpoint/searchTaskByString/Fault/Exception")})
    @RequestWrapper(localName = "searchTaskByString", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.SearchTaskByString")
    @ResponseWrapper(localName = "searchTaskByStringResponse", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.SearchTaskByStringResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ru.evstigneev.tm.api.endpoint.TaskDto> searchTaskByString(
        @WebParam(name = "session", targetNamespace = "")
        ru.evstigneev.tm.api.endpoint.SessionDto session,
        @WebParam(name = "string", targetNamespace = "")
        java.lang.String string
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.tm.evstigneev.ru/ITaskEndpoint/findAllTasksByUserIdRequest", output = "http://endpoint.api.tm.evstigneev.ru/ITaskEndpoint/findAllTasksByUserIdResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.tm.evstigneev.ru/ITaskEndpoint/findAllTasksByUserId/Fault/Exception")})
    @RequestWrapper(localName = "findAllTasksByUserId", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.FindAllTasksByUserId")
    @ResponseWrapper(localName = "findAllTasksByUserIdResponse", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.FindAllTasksByUserIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ru.evstigneev.tm.api.endpoint.TaskDto> findAllTasksByUserId(
        @WebParam(name = "session", targetNamespace = "")
        ru.evstigneev.tm.api.endpoint.SessionDto session
    ) throws Exception_Exception;
}
