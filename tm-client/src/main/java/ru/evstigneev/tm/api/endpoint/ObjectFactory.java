
package ru.evstigneev.tm.api.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.evstigneev.tm.api.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Exception_QNAME = new QName("http://endpoint.api.tm.evstigneev.ru/", "Exception");
    private final static QName _ReadDataJacksonJson_QNAME = new QName("http://endpoint.api.tm.evstigneev.ru/", "readDataJacksonJson");
    private final static QName _ReadDataJacksonJsonResponse_QNAME = new QName("http://endpoint.api.tm.evstigneev.ru/", "readDataJacksonJsonResponse");
    private final static QName _ReadDataJacksonXml_QNAME = new QName("http://endpoint.api.tm.evstigneev.ru/", "readDataJacksonXml");
    private final static QName _ReadDataJacksonXmlResponse_QNAME = new QName("http://endpoint.api.tm.evstigneev.ru/", "readDataJacksonXmlResponse");
    private final static QName _ReadDataJaxbJson_QNAME = new QName("http://endpoint.api.tm.evstigneev.ru/", "readDataJaxbJson");
    private final static QName _ReadDataJaxbJsonResponse_QNAME = new QName("http://endpoint.api.tm.evstigneev.ru/", "readDataJaxbJsonResponse");
    private final static QName _ReadDataJaxbXml_QNAME = new QName("http://endpoint.api.tm.evstigneev.ru/", "readDataJaxbXml");
    private final static QName _ReadDataJaxbXmlResponse_QNAME = new QName("http://endpoint.api.tm.evstigneev.ru/", "readDataJaxbXmlResponse");
    private final static QName _ReadDataSerialization_QNAME = new QName("http://endpoint.api.tm.evstigneev.ru/", "readDataSerialization");
    private final static QName _ReadDataSerializationResponse_QNAME = new QName("http://endpoint.api.tm.evstigneev.ru/", "readDataSerializationResponse");
    private final static QName _WriteDataJacksonJson_QNAME = new QName("http://endpoint.api.tm.evstigneev.ru/", "writeDataJacksonJson");
    private final static QName _WriteDataJacksonJsonResponse_QNAME = new QName("http://endpoint.api.tm.evstigneev.ru/", "writeDataJacksonJsonResponse");
    private final static QName _WriteDataJacksonXml_QNAME = new QName("http://endpoint.api.tm.evstigneev.ru/", "writeDataJacksonXml");
    private final static QName _WriteDataJacksonXmlResponse_QNAME = new QName("http://endpoint.api.tm.evstigneev.ru/", "writeDataJacksonXmlResponse");
    private final static QName _WriteDataJaxbJson_QNAME = new QName("http://endpoint.api.tm.evstigneev.ru/", "writeDataJaxbJson");
    private final static QName _WriteDataJaxbJsonResponse_QNAME = new QName("http://endpoint.api.tm.evstigneev.ru/", "writeDataJaxbJsonResponse");
    private final static QName _WriteDataJaxbXml_QNAME = new QName("http://endpoint.api.tm.evstigneev.ru/", "writeDataJaxbXml");
    private final static QName _WriteDataJaxbXmlResponse_QNAME = new QName("http://endpoint.api.tm.evstigneev.ru/", "writeDataJaxbXmlResponse");
    private final static QName _WriteDataSerialization_QNAME = new QName("http://endpoint.api.tm.evstigneev.ru/", "writeDataSerialization");
    private final static QName _WriteDataSerializationResponse_QNAME = new QName("http://endpoint.api.tm.evstigneev.ru/", "writeDataSerializationResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.evstigneev.tm.api.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Exception }
     * 
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link ReadDataJacksonJson }
     * 
     */
    public ReadDataJacksonJson createReadDataJacksonJson() {
        return new ReadDataJacksonJson();
    }

    /**
     * Create an instance of {@link ReadDataJacksonJsonResponse }
     * 
     */
    public ReadDataJacksonJsonResponse createReadDataJacksonJsonResponse() {
        return new ReadDataJacksonJsonResponse();
    }

    /**
     * Create an instance of {@link ReadDataJacksonXml }
     * 
     */
    public ReadDataJacksonXml createReadDataJacksonXml() {
        return new ReadDataJacksonXml();
    }

    /**
     * Create an instance of {@link ReadDataJacksonXmlResponse }
     * 
     */
    public ReadDataJacksonXmlResponse createReadDataJacksonXmlResponse() {
        return new ReadDataJacksonXmlResponse();
    }

    /**
     * Create an instance of {@link ReadDataJaxbJson }
     * 
     */
    public ReadDataJaxbJson createReadDataJaxbJson() {
        return new ReadDataJaxbJson();
    }

    /**
     * Create an instance of {@link ReadDataJaxbJsonResponse }
     * 
     */
    public ReadDataJaxbJsonResponse createReadDataJaxbJsonResponse() {
        return new ReadDataJaxbJsonResponse();
    }

    /**
     * Create an instance of {@link ReadDataJaxbXml }
     * 
     */
    public ReadDataJaxbXml createReadDataJaxbXml() {
        return new ReadDataJaxbXml();
    }

    /**
     * Create an instance of {@link ReadDataJaxbXmlResponse }
     * 
     */
    public ReadDataJaxbXmlResponse createReadDataJaxbXmlResponse() {
        return new ReadDataJaxbXmlResponse();
    }

    /**
     * Create an instance of {@link ReadDataSerialization }
     * 
     */
    public ReadDataSerialization createReadDataSerialization() {
        return new ReadDataSerialization();
    }

    /**
     * Create an instance of {@link ReadDataSerializationResponse }
     * 
     */
    public ReadDataSerializationResponse createReadDataSerializationResponse() {
        return new ReadDataSerializationResponse();
    }

    /**
     * Create an instance of {@link WriteDataJacksonJson }
     * 
     */
    public WriteDataJacksonJson createWriteDataJacksonJson() {
        return new WriteDataJacksonJson();
    }

    /**
     * Create an instance of {@link WriteDataJacksonJsonResponse }
     * 
     */
    public WriteDataJacksonJsonResponse createWriteDataJacksonJsonResponse() {
        return new WriteDataJacksonJsonResponse();
    }

    /**
     * Create an instance of {@link WriteDataJacksonXml }
     * 
     */
    public WriteDataJacksonXml createWriteDataJacksonXml() {
        return new WriteDataJacksonXml();
    }

    /**
     * Create an instance of {@link WriteDataJacksonXmlResponse }
     * 
     */
    public WriteDataJacksonXmlResponse createWriteDataJacksonXmlResponse() {
        return new WriteDataJacksonXmlResponse();
    }

    /**
     * Create an instance of {@link WriteDataJaxbJson }
     * 
     */
    public WriteDataJaxbJson createWriteDataJaxbJson() {
        return new WriteDataJaxbJson();
    }

    /**
     * Create an instance of {@link WriteDataJaxbJsonResponse }
     * 
     */
    public WriteDataJaxbJsonResponse createWriteDataJaxbJsonResponse() {
        return new WriteDataJaxbJsonResponse();
    }

    /**
     * Create an instance of {@link WriteDataJaxbXml }
     * 
     */
    public WriteDataJaxbXml createWriteDataJaxbXml() {
        return new WriteDataJaxbXml();
    }

    /**
     * Create an instance of {@link WriteDataJaxbXmlResponse }
     * 
     */
    public WriteDataJaxbXmlResponse createWriteDataJaxbXmlResponse() {
        return new WriteDataJaxbXmlResponse();
    }

    /**
     * Create an instance of {@link WriteDataSerialization }
     * 
     */
    public WriteDataSerialization createWriteDataSerialization() {
        return new WriteDataSerialization();
    }

    /**
     * Create an instance of {@link WriteDataSerializationResponse }
     * 
     */
    public WriteDataSerializationResponse createWriteDataSerializationResponse() {
        return new WriteDataSerializationResponse();
    }

    /**
     * Create an instance of {@link SessionDto }
     * 
     */
    public SessionDto createSessionDto() {
        return new SessionDto();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.evstigneev.ru/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReadDataJacksonJson }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.evstigneev.ru/", name = "readDataJacksonJson")
    public JAXBElement<ReadDataJacksonJson> createReadDataJacksonJson(ReadDataJacksonJson value) {
        return new JAXBElement<ReadDataJacksonJson>(_ReadDataJacksonJson_QNAME, ReadDataJacksonJson.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReadDataJacksonJsonResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.evstigneev.ru/", name = "readDataJacksonJsonResponse")
    public JAXBElement<ReadDataJacksonJsonResponse> createReadDataJacksonJsonResponse(ReadDataJacksonJsonResponse value) {
        return new JAXBElement<ReadDataJacksonJsonResponse>(_ReadDataJacksonJsonResponse_QNAME, ReadDataJacksonJsonResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReadDataJacksonXml }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.evstigneev.ru/", name = "readDataJacksonXml")
    public JAXBElement<ReadDataJacksonXml> createReadDataJacksonXml(ReadDataJacksonXml value) {
        return new JAXBElement<ReadDataJacksonXml>(_ReadDataJacksonXml_QNAME, ReadDataJacksonXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReadDataJacksonXmlResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.evstigneev.ru/", name = "readDataJacksonXmlResponse")
    public JAXBElement<ReadDataJacksonXmlResponse> createReadDataJacksonXmlResponse(ReadDataJacksonXmlResponse value) {
        return new JAXBElement<ReadDataJacksonXmlResponse>(_ReadDataJacksonXmlResponse_QNAME, ReadDataJacksonXmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReadDataJaxbJson }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.evstigneev.ru/", name = "readDataJaxbJson")
    public JAXBElement<ReadDataJaxbJson> createReadDataJaxbJson(ReadDataJaxbJson value) {
        return new JAXBElement<ReadDataJaxbJson>(_ReadDataJaxbJson_QNAME, ReadDataJaxbJson.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReadDataJaxbJsonResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.evstigneev.ru/", name = "readDataJaxbJsonResponse")
    public JAXBElement<ReadDataJaxbJsonResponse> createReadDataJaxbJsonResponse(ReadDataJaxbJsonResponse value) {
        return new JAXBElement<ReadDataJaxbJsonResponse>(_ReadDataJaxbJsonResponse_QNAME, ReadDataJaxbJsonResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReadDataJaxbXml }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.evstigneev.ru/", name = "readDataJaxbXml")
    public JAXBElement<ReadDataJaxbXml> createReadDataJaxbXml(ReadDataJaxbXml value) {
        return new JAXBElement<ReadDataJaxbXml>(_ReadDataJaxbXml_QNAME, ReadDataJaxbXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReadDataJaxbXmlResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.evstigneev.ru/", name = "readDataJaxbXmlResponse")
    public JAXBElement<ReadDataJaxbXmlResponse> createReadDataJaxbXmlResponse(ReadDataJaxbXmlResponse value) {
        return new JAXBElement<ReadDataJaxbXmlResponse>(_ReadDataJaxbXmlResponse_QNAME, ReadDataJaxbXmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReadDataSerialization }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.evstigneev.ru/", name = "readDataSerialization")
    public JAXBElement<ReadDataSerialization> createReadDataSerialization(ReadDataSerialization value) {
        return new JAXBElement<ReadDataSerialization>(_ReadDataSerialization_QNAME, ReadDataSerialization.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReadDataSerializationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.evstigneev.ru/", name = "readDataSerializationResponse")
    public JAXBElement<ReadDataSerializationResponse> createReadDataSerializationResponse(ReadDataSerializationResponse value) {
        return new JAXBElement<ReadDataSerializationResponse>(_ReadDataSerializationResponse_QNAME, ReadDataSerializationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WriteDataJacksonJson }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.evstigneev.ru/", name = "writeDataJacksonJson")
    public JAXBElement<WriteDataJacksonJson> createWriteDataJacksonJson(WriteDataJacksonJson value) {
        return new JAXBElement<WriteDataJacksonJson>(_WriteDataJacksonJson_QNAME, WriteDataJacksonJson.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WriteDataJacksonJsonResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.evstigneev.ru/", name = "writeDataJacksonJsonResponse")
    public JAXBElement<WriteDataJacksonJsonResponse> createWriteDataJacksonJsonResponse(WriteDataJacksonJsonResponse value) {
        return new JAXBElement<WriteDataJacksonJsonResponse>(_WriteDataJacksonJsonResponse_QNAME, WriteDataJacksonJsonResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WriteDataJacksonXml }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.evstigneev.ru/", name = "writeDataJacksonXml")
    public JAXBElement<WriteDataJacksonXml> createWriteDataJacksonXml(WriteDataJacksonXml value) {
        return new JAXBElement<WriteDataJacksonXml>(_WriteDataJacksonXml_QNAME, WriteDataJacksonXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WriteDataJacksonXmlResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.evstigneev.ru/", name = "writeDataJacksonXmlResponse")
    public JAXBElement<WriteDataJacksonXmlResponse> createWriteDataJacksonXmlResponse(WriteDataJacksonXmlResponse value) {
        return new JAXBElement<WriteDataJacksonXmlResponse>(_WriteDataJacksonXmlResponse_QNAME, WriteDataJacksonXmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WriteDataJaxbJson }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.evstigneev.ru/", name = "writeDataJaxbJson")
    public JAXBElement<WriteDataJaxbJson> createWriteDataJaxbJson(WriteDataJaxbJson value) {
        return new JAXBElement<WriteDataJaxbJson>(_WriteDataJaxbJson_QNAME, WriteDataJaxbJson.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WriteDataJaxbJsonResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.evstigneev.ru/", name = "writeDataJaxbJsonResponse")
    public JAXBElement<WriteDataJaxbJsonResponse> createWriteDataJaxbJsonResponse(WriteDataJaxbJsonResponse value) {
        return new JAXBElement<WriteDataJaxbJsonResponse>(_WriteDataJaxbJsonResponse_QNAME, WriteDataJaxbJsonResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WriteDataJaxbXml }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.evstigneev.ru/", name = "writeDataJaxbXml")
    public JAXBElement<WriteDataJaxbXml> createWriteDataJaxbXml(WriteDataJaxbXml value) {
        return new JAXBElement<WriteDataJaxbXml>(_WriteDataJaxbXml_QNAME, WriteDataJaxbXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WriteDataJaxbXmlResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.evstigneev.ru/", name = "writeDataJaxbXmlResponse")
    public JAXBElement<WriteDataJaxbXmlResponse> createWriteDataJaxbXmlResponse(WriteDataJaxbXmlResponse value) {
        return new JAXBElement<WriteDataJaxbXmlResponse>(_WriteDataJaxbXmlResponse_QNAME, WriteDataJaxbXmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WriteDataSerialization }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.evstigneev.ru/", name = "writeDataSerialization")
    public JAXBElement<WriteDataSerialization> createWriteDataSerialization(WriteDataSerialization value) {
        return new JAXBElement<WriteDataSerialization>(_WriteDataSerialization_QNAME, WriteDataSerialization.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WriteDataSerializationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.evstigneev.ru/", name = "writeDataSerializationResponse")
    public JAXBElement<WriteDataSerializationResponse> createWriteDataSerializationResponse(WriteDataSerializationResponse value) {
        return new JAXBElement<WriteDataSerializationResponse>(_WriteDataSerializationResponse_QNAME, WriteDataSerializationResponse.class, null, value);
    }

}
