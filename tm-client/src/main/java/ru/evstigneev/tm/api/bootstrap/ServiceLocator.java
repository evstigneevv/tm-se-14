package ru.evstigneev.tm.api.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.api.endpoint.*;
import ru.evstigneev.tm.command.AbstractCommand;

import java.lang.Exception;
import java.util.Collection;
import java.util.Scanner;

public interface ServiceLocator {

    @NotNull IUserEndpoint getUserEndpoint();

    void setUserEndpoint(@NotNull final IUserEndpoint userEndpoint);

    @NotNull IProjectEndpoint getProjectEndpoint();

    void setProjectEndpoint(@NotNull final IProjectEndpoint projectEndpoint);

    @NotNull ITaskEndpoint getTaskEndpoint();

    void setTaskEndpoint(@NotNull final ITaskEndpoint taskEndpoint);

    @NotNull ISessionEndpoint getSessionEndpoint();

    void setSessionEndpoint(@NotNull final ISessionEndpoint sessionEndpoint);

    @NotNull Scanner getScanner();

    @NotNull Collection<AbstractCommand> getCommands();

    void init() throws Exception;

    @NotNull SessionDto getSession();

    void setSession(@Nullable final SessionDto currentSession);

    @NotNull IDomainEndpoint getDomainEndpoint();

    void setDomainEndpoint(@Nullable final IDomainEndpoint domainEndpoint);

}
