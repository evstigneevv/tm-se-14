package ru.evstigneev.tm.command.task;

import ru.evstigneev.tm.api.endpoint.TaskDto;
import ru.evstigneev.tm.command.AbstractCommand;

public class TaskGetAllCommand extends AbstractCommand {

    @Override
    public String command() {
        return "SAT";
    }

    @Override
    public String description() {
        return "Show all tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("SHOW ALL TASKS");
        for (TaskDto task : bootstrap.getTaskEndpoint().findAllTasks(bootstrap.getSession())) {
            System.out.println("User ID: " + task.getUserId() + " | Project ID: " + task.getProjectId() + " | Task ID:" + task.getId() + " | Task name: "
                    + task.getName() + " | Task description: " + task.getDescription() + " | Date of creation: "
                    + task.getDateOfCreation() + " | Date of start: " + task.getDateStart() + " | Date of finish: "
                    + task.getDateFinish() + " | Task status: " + task.getStatus());
        }
        System.out.println();
    }

}
