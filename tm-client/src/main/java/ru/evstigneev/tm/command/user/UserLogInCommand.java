package ru.evstigneev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.command.AbstractCommand;

public class UserLogInCommand extends AbstractCommand {

    @Override
    public String command() {
        return "LI";
    }

    @Override
    public String description() {
        return "Changes current user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("LOG IN");
        System.out.println("enter user login");
        @NotNull final String login = bootstrap.getScanner().nextLine();
        System.out.println("enter user password");
        @NotNull final String password = bootstrap.getScanner().nextLine();
        if (bootstrap.getUserEndpoint().checkPassword(login, password)) {
            bootstrap.setSession(bootstrap.getSessionEndpoint().openSession(login, password));
            System.out.println("You successfully logged in!");
        } else {
            System.out.println("Wrong password!");
        }
    }

}
