package ru.evstigneev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.api.endpoint.TaskDto;
import ru.evstigneev.tm.command.AbstractCommand;

import java.util.Collection;

public class TaskGetCommand extends AbstractCommand {

    @Override
    public String command() {
        return "ST";
    }

    @Override
    public String description() {
        return "Shows all tasks for specific project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("input project ID: ");
        @NotNull final Collection<TaskDto> taskList =
                bootstrap.getTaskEndpoint().getTaskListByProjectId(bootstrap.getSession(),
                        bootstrap.getScanner().nextLine());
        for (TaskDto task : taskList) {
            System.out.println("User ID: " + task.getUserId() + " | Project ID: " + task.getProjectId() + " | Task ID:" + task.getId() + " | Task name: "
                    + task.getName() + " | Task description: " + task.getDescription() + " | Date of creation: "
                    + task.getDateOfCreation() + " | Date of start: " + task.getDateStart() + " | Date of finish: "
                    + task.getDateFinish() + " | Task status: " + task.getStatus());
        }
        System.out.println();
    }

}
