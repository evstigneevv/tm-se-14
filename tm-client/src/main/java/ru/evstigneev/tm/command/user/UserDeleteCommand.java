package ru.evstigneev.tm.command.user;

import ru.evstigneev.tm.command.AbstractCommand;

public class UserDeleteCommand extends AbstractCommand {

    @Override
    public String command() {
        return "DU";
    }

    @Override
    public String description() {
        return "Delete user by ID";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("DELETE USER");
        System.out.println("Enter user ID: ");
        bootstrap.getUserEndpoint().removeUser(bootstrap.getSession(), bootstrap.getScanner().nextLine());
        System.out.println("User was deleted!");
    }

}
