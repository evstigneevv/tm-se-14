package ru.evstigneev.tm.command.project;

import ru.evstigneev.tm.api.endpoint.ProjectDto;
import ru.evstigneev.tm.command.AbstractCommand;

public class ProjectGetAllCommand extends AbstractCommand {

    @Override
    public String command() {
        return "SAP";
    }

    @Override
    public String description() {
        return "Show all projects";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("SHOW ALL PROJECTS");
        for (ProjectDto project : bootstrap.getProjectEndpoint().findAllProjects(bootstrap.getSession())) {
            System.out.println("User ID: " + project.getUserId() + " | Project ID: " + project.getId()
                    + " | Project name: " + project.getName() + " | Project description: " + project.getDescription()
                    + " | Date of creation: " + project.getDateOfCreation() + " | Date of start: "
                    + project.getDateStart() + " | Date of finish: " + project.getDateFinish() + " | Project status: "
                    + project.getStatus());
        }
        System.out.println();
    }

}
