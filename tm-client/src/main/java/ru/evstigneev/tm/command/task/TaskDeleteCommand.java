package ru.evstigneev.tm.command.task;

import ru.evstigneev.tm.command.AbstractCommand;

public class TaskDeleteCommand extends AbstractCommand {

    @Override
    public String command() {
        return "DT";
    }

    @Override
    public String description() {
        return "Delete task";
    }

    @Override
    public void execute() throws Exception {
        bootstrap.getTaskEndpoint().removeAllTasksByUserId(bootstrap.getSession());
        System.out.println("Tasks was deleted!");
    }

}
