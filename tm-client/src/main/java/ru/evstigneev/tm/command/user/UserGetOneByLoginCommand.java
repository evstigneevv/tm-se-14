package ru.evstigneev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.api.endpoint.UserDto;
import ru.evstigneev.tm.command.AbstractCommand;

public class UserGetOneByLoginCommand extends AbstractCommand {

    @Override
    public String command() {
        return "SOU";
    }

    @Override
    public String description() {
        return "Show founded by login user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("SHOW USER");
        System.out.println("Enter user login: ");
        @NotNull final String login = bootstrap.getScanner().nextLine();
        UserDto user = bootstrap.getUserEndpoint().findUserByLogin(bootstrap.getSession(), login);
        System.out.println("login: " + user.getLogin() + " | ID: " + user.getId() + " | ROLE: " + user.getRole());
    }

}
