package ru.evstigneev.tm.command.project;

import ru.evstigneev.tm.api.endpoint.ProjectDto;
import ru.evstigneev.tm.command.AbstractCommand;

public class ProjectGetAllByUserCommand extends AbstractCommand {

    @Override
    public String command() {
        return "SUP";
    }

    @Override
    public String description() {
        return "Show all projects of specify user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("SHOW PROJECTS BY USER ID");
        for (ProjectDto project : bootstrap.getProjectEndpoint().findAllProjectsByUserId(bootstrap.getSession())) {
            System.out.println("User ID: " + project.getUserId() + " | Project ID: " + project.getId()
                    + " | Project name: " + project.getName() + " | Project description: " + project.getDescription()
                    + " | Date of creation: "
                    + project.getDateOfCreation() + " | Date of start: " + project.getDateStart() + " | Date of finish: "
                    + project.getDateFinish() + " | Project status: " + project.getStatus());
        }
        System.out.println();
    }

}
