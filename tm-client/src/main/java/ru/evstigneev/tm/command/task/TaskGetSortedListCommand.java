package ru.evstigneev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.api.endpoint.TaskDto;
import ru.evstigneev.tm.command.AbstractCommand;

public class TaskGetSortedListCommand extends AbstractCommand {

    @Override
    public String command() {
        return "TGSL";
    }

    @Override
    public String description() {
        return "Show sorted by you choice list of tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("TASK SORT BY VALUE");
        System.out.println("Enter by which value sort tasks: ");
        @NotNull final String comparatorName = bootstrap.getScanner().nextLine();
        for (TaskDto task : bootstrap.getTaskEndpoint().sortTasks(bootstrap.getSession(), comparatorName)) {
            System.out.println("User ID: " + task.getUserId() + " | Project ID: " + task.getProjectId() + " | Task ID:" + task.getId() + " | Task name: "
                    + task.getName() + " | Task description: " + task.getDescription() + " | Date of creation: "
                    + task.getDateOfCreation() + " | Date of start: " + task.getDateStart() + " | Date of finish: "
                    + task.getDateFinish() + " | Task status: " + task.getStatus());
        }
        System.out.println();
    }

}
