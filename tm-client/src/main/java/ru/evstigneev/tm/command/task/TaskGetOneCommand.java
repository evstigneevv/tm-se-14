package ru.evstigneev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.api.endpoint.TaskDto;
import ru.evstigneev.tm.command.AbstractCommand;

public class TaskGetOneCommand extends AbstractCommand {

    @Override
    public String command() {
        return "SOT";
    }

    @Override
    public String description() {
        return "Shows specify task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("input project ID: ");
        @NotNull final String projectId = bootstrap.getScanner().nextLine();
        System.out.println("input task ID: ");
        @NotNull final String taskId = bootstrap.getScanner().nextLine();
        @NotNull final TaskDto task = bootstrap.getTaskEndpoint().findOne(bootstrap.getSession(), projectId, taskId);
        System.out.println("User ID: " + task.getUserId() + " | Project ID: " + task.getProjectId() + " | Task ID:" + task.getId() + " | Task name: "
                + task.getName() + " | Task description: " + task.getDescription() + " | Date of creation: "
                + task.getDateOfCreation() + " | Date of start: " + task.getDateStart() + " | Date of finish: "
                + task.getDateFinish() + " | Task status: " + task.getStatus());
        System.out.println();
    }

}
