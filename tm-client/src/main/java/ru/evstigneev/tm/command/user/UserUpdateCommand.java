package ru.evstigneev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.api.endpoint.RoleType;
import ru.evstigneev.tm.command.AbstractCommand;

public class UserUpdateCommand extends AbstractCommand {

    @Override
    public String command() {
        return "UU";
    }

    @Override
    public String description() {
        return "Update current user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("UPDATE USER");
        System.out.println("enter new user name: ");
        @NotNull final String login = bootstrap.getScanner().nextLine();
        System.out.println("enter new password: ");
        @NotNull final String password = bootstrap.getScanner().nextLine();
        System.out.println("enter new role: ");
        bootstrap.getUserEndpoint().updateUser(bootstrap.getSession(), login, password,
                RoleType.valueOf(bootstrap.getScanner().nextLine().toUpperCase()));
        System.out.println("User was changed!");
    }

}

