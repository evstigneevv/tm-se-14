package ru.evstigneev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.command.AbstractCommand;

public class ProjectDeleteCommand extends AbstractCommand {

    @Override
    public String command() {
        return "DP";
    }

    @Override
    public String description() {
        return "Delete specify project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("DELETE SPECIFY PROJECT");
        System.out.println("Enter project ID: ");
        @NotNull final String projectId = bootstrap.getScanner().nextLine();
        bootstrap.getProjectEndpoint().removeProject(bootstrap.getSession(), projectId);
        System.out.println("Project was deleted!");
    }

}