package ru.evstigneev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.api.endpoint.Status;
import ru.evstigneev.tm.command.AbstractCommand;

public class TaskUpdateCommand extends AbstractCommand {

    @Override
    public String command() {
        return "TU";
    }

    @Override
    public String description() {
        return "Update task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("enter task ID: ");
        @NotNull final String taskId = bootstrap.getScanner().nextLine();
        System.out.println("enter task name: ");
        @NotNull final String taskName = bootstrap.getScanner().nextLine();
        System.out.println("enter task description: ");
        @NotNull final String description = bootstrap.getScanner().nextLine();
        System.out.println("enter task start date: ");
        @NotNull final String dateStart = bootstrap.getScanner().nextLine();
        System.out.println("enter task finish date: ");
        @NotNull final String dateFinish = bootstrap.getScanner().nextLine();
        System.out.println("enter task status: ");
        @NotNull final String status = bootstrap.getScanner().nextLine();
        bootstrap.getTaskEndpoint().updateTask(bootstrap.getSession(), taskId, taskName, description, dateStart,
                dateFinish, Status.valueOf(status));
        System.out.println("TASK UPDATED!");
        System.out.println();
    }

}
