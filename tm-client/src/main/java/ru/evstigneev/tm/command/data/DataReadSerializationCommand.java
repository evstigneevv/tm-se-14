package ru.evstigneev.tm.command.data;

import ru.evstigneev.tm.command.AbstractCommand;

public class DataReadSerializationCommand extends AbstractCommand {

    @Override
    public String command() {
        return "DATA READ";
    }

    @Override
    public String description() {
        return "Read data from file";
    }

    @Override
    public void execute() throws Exception {
        bootstrap.getDomainEndpoint().readDataSerialization(bootstrap.getSession());
    }

}
