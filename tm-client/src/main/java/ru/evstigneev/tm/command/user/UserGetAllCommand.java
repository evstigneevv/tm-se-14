package ru.evstigneev.tm.command.user;

import ru.evstigneev.tm.api.endpoint.UserDto;
import ru.evstigneev.tm.command.AbstractCommand;

public class UserGetAllCommand extends AbstractCommand {

    @Override
    public String command() {
        return "SAU";
    }

    @Override
    public String description() {
        return "Show all users";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("SHOW ALL USERS");
        for (UserDto user : bootstrap.getUserEndpoint().findAllUsers(bootstrap.getSession())) {
            System.out.println("login: " + user.getLogin() + " | ID: " + user.getId() + " | ROLE: " + user.getRole());
        }
    }

}
