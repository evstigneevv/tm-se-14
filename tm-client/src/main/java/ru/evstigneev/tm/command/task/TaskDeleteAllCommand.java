package ru.evstigneev.tm.command.task;

import ru.evstigneev.tm.command.AbstractCommand;

public class TaskDeleteAllCommand extends AbstractCommand {

    @Override
    public String command() {
        return "DAT";
    }

    @Override
    public String description() {
        return "Delete all tasks!";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("DELETE ALL TASKS");
        bootstrap.getTaskEndpoint().removeAllTasks(bootstrap.getSession());
        System.out.println("All tasks deleted!");
    }

}
