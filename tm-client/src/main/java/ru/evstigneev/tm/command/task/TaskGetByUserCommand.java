package ru.evstigneev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.api.endpoint.TaskDto;
import ru.evstigneev.tm.command.AbstractCommand;

public class TaskGetByUserCommand extends AbstractCommand {

    @Override
    public String command() {
        return "SUT";
    }

    @Override
    public String description() {
        return "Show all user tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Enter user ID: ");
        @NotNull final String userId = bootstrap.getScanner().nextLine();
        for (TaskDto task : bootstrap.getTaskEndpoint().findAllTasksByUserId(bootstrap.getSession())) {
            System.out.println("User ID: " + task.getUserId() + " | Project ID: " + task.getProjectId() + " | Task ID:" + task.getId() + " | Task name: "
                    + task.getName() + " | Task description: " + task.getDescription() + " | Date of creation: "
                    + task.getDateOfCreation() + " | Date of start: " + task.getDateStart() + " | Date of finish: "
                    + task.getDateFinish() + " | Task status: " + task.getStatus());
        }
        System.out.println();
    }

}
