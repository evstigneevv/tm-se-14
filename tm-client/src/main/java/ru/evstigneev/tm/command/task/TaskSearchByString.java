package ru.evstigneev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.api.endpoint.TaskDto;
import ru.evstigneev.tm.command.AbstractCommand;

public class TaskSearchByString extends AbstractCommand {

    @Override
    public String command() {
        return "STS";
    }

    @Override
    public String description() {
        return "Search tasks by string";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Enter a string for search: ");
        @NotNull final String searchingString = bootstrap.getScanner().nextLine();
        for (TaskDto task : bootstrap.getTaskEndpoint().searchTaskByString(bootstrap.getSession(), searchingString)) {
            System.out.println("User ID: " + task.getUserId() + " | Project ID: " + task.getProjectId() + " | Task ID:" + task.getId() + " | Task name: "
                    + task.getName() + " | Task description: " + task.getDescription() + " | Date of creation: "
                    + task.getDateOfCreation() + " | Date of start: " + task.getDateStart() + " | Date of finish: "
                    + task.getDateFinish() + " | Task status: " + task.getStatus());
        }
        System.out.println();
    }

}
